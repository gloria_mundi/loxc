
#ifndef LOXC_MACROS_H
#define LOXC_MACROS_H

#ifdef NDEBUG
#	ifdef __GNUC__
#		define unreachable() __builtin_unreachable()
#	else
#		define unreachable() ((void) 0)
#	endif
#else
#	include <stdio.h>
#	include <stdlib.h>
#	define unreachable() ( \
		fprintf(stderr, "%s:%d: %s: Unreachable code reached\n", \
		  __FILE__, __LINE__, __func__), \
		abort() \
	)
#endif

#endif
