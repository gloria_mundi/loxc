
#ifndef LOXC_METADATA_H
#define LOXC_METADATA_H

typedef struct loxc_metadata *loxc_metadata_t;
typedef void (*loxc_metadata_key_t)(void *);

loxc_metadata_t loxc_metadata_new(void);
void loxc_metadata_free(loxc_metadata_t);
void *loxc_metadata_set(loxc_metadata_t, loxc_metadata_key_t key, void *value);
void *loxc_metadata_get(loxc_metadata_t, loxc_metadata_key_t key);

#endif
