
#ifndef LOXC_TYPE_H
#define LOXC_TYPE_H

#include <stdbool.h>
#include <stdint.h>

typedef enum {
	LOXC_PRIMITIVE_NUMBER,
	LOXC_PRIMITIVE_BOOL,
	LOXC_PRIMITIVE_NIL,

	LOXC_N_PRIMITIVES,
} loxc_primitive_t;

#define LOXC_PRIMITIVE_SET(prim) (1 << (prim))
#define LOXC_PRIMITIVE_SET_EMPTY    0
#define LOXC_PRIMITIVE_SET_NUMBER   LOXC_PRIMITIVE_SET(LOXC_PRIMITIVE_NUMBER)
#define LOXC_PRIMITIVE_SET_BOOL     LOXC_PRIMITIVE_SET(LOXC_PRIMITIVE_BOOL)
#define LOXC_PRIMITIVE_SET_NIL      LOXC_PRIMITIVE_SET(LOXC_PRIMITIVE_NIL)

typedef uint_least8_t loxc_primitive_set_t;
typedef struct loxc_typeset *loxc_typeset_t;

loxc_typeset_t loxc_typeset_empty(void);
loxc_typeset_t loxc_typeset_primitive(loxc_primitive_t);
loxc_typeset_t loxc_typeset_union(loxc_typeset_t, loxc_typeset_t);
loxc_typeset_t loxc_typeset_limit_falsy(loxc_typeset_t);
loxc_typeset_t loxc_typeset_limit_truthy(loxc_typeset_t);

void loxc_typeset_ref(loxc_typeset_t);
void loxc_typeset_unref(loxc_typeset_t);

bool loxc_typeset_is_primitive(loxc_typeset_t);
bool loxc_typeset_is_empty(loxc_typeset_t);

loxc_primitive_set_t loxc_typeset_primitive_set(loxc_typeset_t);

#endif
