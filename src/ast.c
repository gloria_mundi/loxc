
#include <stdbool.h>

#include "ast.h"
#include "mem.h"
#include "string.h"
#include "metadata.h"

/* Expressions */
static loxc_expr_t init_expr(loxc_expr_t expr,
  loxc_span_t position, loxc_expr_type_t type) {
	expr->metadata = loxc_metadata_new();
	expr->position = position;
	expr->type = type;
	return expr;
}

loxc_expr_t loxc_expr_new_nil(loxc_span_t position) {
	loxc_expr_t expr = mem_new(struct loxc_expr);
	return init_expr(expr, position, LOXC_EXPR_NIL);
}

loxc_expr_t loxc_expr_new_bool(loxc_span_t position, bool value) {
	loxc_expr_bool_t boolean = mem_new(struct loxc_expr_bool);
	boolean->value = value;
	return init_expr(&boolean->expr, position, LOXC_EXPR_BOOL);
}

loxc_expr_t loxc_expr_new_number(loxc_span_t position, double value) {
	loxc_expr_number_t number = mem_new(struct loxc_expr_number);
	number->value = value;
	return init_expr(&number->expr, position, LOXC_EXPR_NUMBER);
}

loxc_expr_t loxc_expr_new_binary(loxc_span_t position,
  loxc_expr_binary_op_t op, loxc_expr_t left, loxc_expr_t right) {
	loxc_expr_binary_t binary = mem_new(struct loxc_expr_binary);
	binary->op = op;
	binary->left = left;
	binary->right = right;
	return init_expr(&binary->expr, position, LOXC_EXPR_BINARY);
}

loxc_expr_t loxc_expr_new_unary(loxc_span_t position,
  loxc_expr_unary_op_t op, loxc_expr_t arg) {
	loxc_expr_unary_t unary = mem_new(struct loxc_expr_unary);
	unary->op = op;
	unary->arg = arg;
	return init_expr(&unary->expr, position, LOXC_EXPR_UNARY);
}

loxc_expr_t loxc_expr_new_variable(loxc_span_t position, loxc_string_t name) {
	loxc_expr_variable_t variable = mem_new(struct loxc_expr_variable);
	variable->name = name;
	return init_expr(&variable->expr, position, LOXC_EXPR_VARIABLE);
}

loxc_expr_t loxc_expr_new_assign(loxc_span_t position,
  loxc_string_t name, loxc_expr_t value) {
	loxc_expr_assign_t assign = mem_new(struct loxc_expr_assign);
	assign->name = name;
	assign->value = value;
	return init_expr(&assign->expr, position, LOXC_EXPR_ASSIGN);
}

void loxc_expr_free(loxc_expr_t expr) {
	loxc_metadata_free(expr->metadata);

	switch (expr->type) {
		case LOXC_EXPR_NIL: break;
		case LOXC_EXPR_BOOL: break;
		case LOXC_EXPR_NUMBER: break;
		case LOXC_EXPR_BINARY: {
			loxc_expr_binary_t binary = (loxc_expr_binary_t) expr;
			loxc_expr_free(binary->left);
			loxc_expr_free(binary->right);
			break;
		}
		case LOXC_EXPR_UNARY: {
			loxc_expr_unary_t unary = (loxc_expr_unary_t) expr;
			loxc_expr_free(unary->arg);
			break;
		}
		case LOXC_EXPR_VARIABLE: {
			loxc_expr_variable_t variable =
			  (loxc_expr_variable_t) expr;
			loxc_string_free(variable->name);
			break;
		}
		case LOXC_EXPR_ASSIGN: {
			loxc_expr_assign_t assign = (loxc_expr_assign_t) expr;
			loxc_string_free(assign->name);
			loxc_expr_free(assign->value);
			break;
		}
	}

	mem_free(expr);
}

static void expr_to_string_helper(loxc_string_builder_t *builder,
  loxc_expr_t expr) {
	switch (expr->type) {
		case LOXC_EXPR_NIL:
			loxc_string_builder_append(builder,
			  loxc_string_new("nil", 3));
			break;
		case LOXC_EXPR_BOOL: {
			loxc_expr_bool_t boolean = (loxc_expr_bool_t) expr;
			if (boolean->value == true) {
				loxc_string_builder_append(builder,
				  loxc_string_new("true", 4));
			} else {
				loxc_string_builder_append(builder,
				  loxc_string_new("false", 5));
			}
			break;
		}
		case LOXC_EXPR_NUMBER: {
			loxc_expr_number_t number = (loxc_expr_number_t) expr;
			loxc_string_builder_appendf(builder, "%g",
			  number->value);
			break;
		}
		case LOXC_EXPR_BINARY: {
			loxc_expr_binary_t binary = (loxc_expr_binary_t) expr;
			static const loxc_string_t ops[] = {
				[LOXC_EXPR_ADD] = { 1, "+" },
				[LOXC_EXPR_SUB] = { 1, "-" },
				[LOXC_EXPR_MUL] = { 1, "*" },
				[LOXC_EXPR_DIV] = { 1, "/" },

				[LOXC_EXPR_LT] = { 1, "<" },
				[LOXC_EXPR_LE] = { 2, "<=" },
				[LOXC_EXPR_GT] = { 1, ">" },
				[LOXC_EXPR_GE] = { 2, ">=" },

				[LOXC_EXPR_EQ] = { 2, "==" },
				[LOXC_EXPR_NE] = { 2, "!=" },

				[LOXC_EXPR_AND] = { 3, "and"},
				[LOXC_EXPR_OR] = { 2, "or"},
			};
			loxc_string_builder_append(builder,
			  loxc_string_new("(", 1));
			loxc_string_builder_append(builder, ops[binary->op]);
			loxc_string_builder_append(builder,
			  loxc_string_new(" ", 1));
			expr_to_string_helper(builder, binary->left);
			loxc_string_builder_append(builder,
			  loxc_string_new(" ", 1));
			expr_to_string_helper(builder, binary->right);
			loxc_string_builder_append(builder,
			  loxc_string_new(")", 1));
			break;
		}
		case LOXC_EXPR_UNARY: {
			loxc_expr_unary_t unary = (loxc_expr_unary_t) expr;
			static const loxc_string_t ops[] = {
				[LOXC_EXPR_NEG] = { 1, "-" },
				[LOXC_EXPR_NOT] = { 1, "!" },
			};
			loxc_string_builder_append(builder,
			  loxc_string_new("(", 1));
			loxc_string_builder_append(builder, ops[unary->op]);
			loxc_string_builder_append(builder,
			  loxc_string_new(" ", 1));
			expr_to_string_helper(builder, unary->arg);
			loxc_string_builder_append(builder,
			  loxc_string_new(")", 1));
			break;
		}
		case LOXC_EXPR_VARIABLE: {
			loxc_expr_variable_t variable =
			  (loxc_expr_variable_t) expr;
			loxc_string_builder_append(builder, variable->name);
			break;
		}
		case LOXC_EXPR_ASSIGN: {
			loxc_expr_assign_t assign = (loxc_expr_assign_t) expr;
			loxc_string_builder_append(builder,
			  loxc_string_new("(= ", 3));
			loxc_string_builder_append(builder, assign->name);
			loxc_string_builder_append(builder,
			  loxc_string_new(" ", 1));
			expr_to_string_helper(builder, assign->value);
			loxc_string_builder_append(builder,
			  loxc_string_new(")", 1));
			break;
		}
	}
}

loxc_string_t loxc_expr_to_string(loxc_expr_t expr) {
	loxc_string_builder_t builder;
	loxc_string_builder_init(&builder);
	expr_to_string_helper(&builder, expr);
	return loxc_string_builder_complete(&builder);
}

/* Statements */
static loxc_stmt_t init_stmt(loxc_stmt_t stmt,
  loxc_span_t position, loxc_stmt_type_t type) {
	stmt->metadata = loxc_metadata_new();
	stmt->position = position;
	stmt->type = type;
	return stmt;
}

loxc_stmt_t loxc_stmt_new_null(loxc_span_t position) {
	return init_stmt(mem_new(struct loxc_stmt), position, LOXC_STMT_NULL);
}

loxc_stmt_t loxc_stmt_new_expr(loxc_span_t position, loxc_expr_t expr) {
	loxc_stmt_expr_t stmt = mem_new(struct loxc_stmt_expr);
	stmt->expr = expr;
	return init_stmt(&stmt->stmt, position, LOXC_STMT_EXPR);
}

loxc_stmt_t loxc_stmt_new_print(loxc_span_t position, loxc_expr_t expr) {
	loxc_stmt_print_t print = mem_new(struct loxc_stmt_print);
	print->expr = expr;
	return init_stmt(&print->stmt, position, LOXC_STMT_PRINT);
}

loxc_stmt_t loxc_stmt_new_declare(loxc_span_t position,
  loxc_string_t name, loxc_expr_t value) {
	loxc_stmt_declare_t declare = mem_new(struct loxc_stmt_declare);
	declare->name = name;
	declare->value = value;
	return init_stmt(&declare->stmt, position, LOXC_STMT_DECLARE);
}

loxc_stmt_t loxc_stmt_new_block(loxc_span_t position, loxc_ast_t inner) {
	loxc_stmt_block_t block = mem_new(struct loxc_stmt_block);
	block->inner = inner;
	return init_stmt(&block->stmt, position, LOXC_STMT_BLOCK);
}

loxc_stmt_t loxc_stmt_new_if(loxc_span_t position,
  loxc_expr_t condition, loxc_stmt_t if_, loxc_stmt_t else_) {
	loxc_stmt_if_t if_stmt = mem_new(struct loxc_stmt_if);
	if_stmt->condition = condition;
	if_stmt->if_ = if_;
	if_stmt->else_ = else_;
	return init_stmt(&if_stmt->stmt, position, LOXC_STMT_IF);
}

void loxc_stmt_free(loxc_stmt_t stmt) {
	loxc_metadata_free(stmt->metadata);
	switch (stmt->type) {
		case LOXC_STMT_NULL: break;
		case LOXC_STMT_EXPR: {
			loxc_stmt_expr_t expr = (loxc_stmt_expr_t) stmt;
			loxc_expr_free(expr->expr);
			break;
		}
		case LOXC_STMT_PRINT: {
			loxc_stmt_print_t print = (loxc_stmt_print_t) stmt;
			loxc_expr_free(print->expr);
			break;
		}
		case LOXC_STMT_DECLARE: {
			loxc_stmt_declare_t declare =
			  (loxc_stmt_declare_t) stmt;
			loxc_string_free(declare->name);
			loxc_expr_free(declare->value);
			break;
		}
		case LOXC_STMT_BLOCK: {
			loxc_stmt_block_t block = (loxc_stmt_block_t) stmt;
			loxc_ast_free(block->inner);
			break;
		}
		case LOXC_STMT_IF: {
			loxc_stmt_if_t if_ = (loxc_stmt_if_t) stmt;
			loxc_expr_free(if_->condition);
			loxc_stmt_free(if_->if_);
			loxc_stmt_free(if_->else_);
			break;
		}
	}
	mem_free(stmt);
}

static void ast_to_string_helper(loxc_string_builder_t *builder,
  loxc_ast_t ast);

static void stmt_to_string_helper(loxc_string_builder_t *builder,
  loxc_stmt_t stmt) {
	switch (stmt->type) {
		case LOXC_STMT_NULL:
			loxc_string_builder_append(builder,
			  loxc_string_new("null", 4));
			break;
		case LOXC_STMT_EXPR: {
			loxc_stmt_expr_t expr = (loxc_stmt_expr_t) stmt;
			expr_to_string_helper(builder, expr->expr);
			break;
		}
		case LOXC_STMT_PRINT: {
			loxc_stmt_print_t print = (loxc_stmt_print_t) stmt;
			loxc_string_builder_append(builder,
			  loxc_string_new("print ", 6));
			expr_to_string_helper(builder, print->expr);
			break;
		}
		case LOXC_STMT_DECLARE: {
			loxc_stmt_declare_t declare =
			  (loxc_stmt_declare_t) stmt;
			loxc_string_builder_append(builder,
			  loxc_string_new("var (= ", 7));
			loxc_string_builder_append(builder, declare->name);
			loxc_string_builder_append(builder,
			  loxc_string_new(" ", 1));
			expr_to_string_helper(builder, declare->value);
			loxc_string_builder_append(builder,
			  loxc_string_new(")", 1));
			break;
		}
		case LOXC_STMT_BLOCK: {
			loxc_stmt_block_t block = (loxc_stmt_block_t) stmt;
			loxc_string_builder_append(builder,
			  loxc_string_new("block (\n", 8));
			ast_to_string_helper(builder, block->inner);
			loxc_string_builder_append(builder,
			  loxc_string_new(")", 1));
			break;
		}
		case LOXC_STMT_IF: {
			loxc_stmt_if_t if_ = (loxc_stmt_if_t) stmt;
			loxc_string_builder_append(builder,
			  loxc_string_new("if ", 3));
			expr_to_string_helper(builder, if_->condition);
			loxc_string_builder_append(builder,
			  loxc_string_new(" (", 2));
			stmt_to_string_helper(builder, if_->if_);
			loxc_string_builder_append(builder,
			  loxc_string_new(") (", 3));
			stmt_to_string_helper(builder, if_->else_);
			loxc_string_builder_append(builder,
			  loxc_string_new(")", 1));
			break;
		}
	}
}

loxc_string_t loxc_stmt_to_string(loxc_stmt_t stmt) {
	loxc_string_builder_t builder;
	loxc_string_builder_init(&builder);
	stmt_to_string_helper(&builder, stmt);
	return loxc_string_builder_complete(&builder);
}

/* AST */
struct loxc_ast {
	loxc_metadata_t metadata;
	unsigned int len, cap;
	loxc_stmt_t *stmts;
};

loxc_ast_t loxc_ast_new(void) {
	loxc_ast_t ast = mem_new(struct loxc_ast);
	ast->metadata = loxc_metadata_new();
	ast->len = ast->cap = 0;
	ast->stmts = NULL;
	return ast;
}

void loxc_ast_append_stmt(loxc_ast_t ast, loxc_stmt_t stmt) {
	if (ast->len == ast->cap) {
		unsigned int new_cap;
		if (ast->cap == 0) new_cap = 0x10;
		else new_cap = 2 * ast->cap;
		ast->stmts = mem_resize_array(ast->stmts, new_cap);
		ast->cap = new_cap;
	}

	ast->stmts[ast->len++] = stmt;
}

unsigned int loxc_ast_count_stmts(loxc_ast_t ast) {
	return ast->len;
}

loxc_stmt_t loxc_ast_get_stmt(loxc_ast_t ast, unsigned int index) {
	return ast->stmts[index];
}

loxc_metadata_t loxc_ast_metadata(loxc_ast_t ast) {
	return ast->metadata;
}

void loxc_ast_free(loxc_ast_t ast) {
	loxc_metadata_free(ast->metadata);
	for (unsigned int i = 0; i < ast->len; i++)
	  loxc_stmt_free(ast->stmts[i]);
	mem_free(ast->stmts);
	mem_free(ast);
}

static void ast_to_string_helper(loxc_string_builder_t *builder,
  loxc_ast_t ast) {
	for (unsigned int i = 0; i < ast->len; i++) {
		stmt_to_string_helper(builder, ast->stmts[i]);
		loxc_string_builder_append(builder, loxc_string_new("\n", 1));
	}
}

loxc_string_t loxc_ast_to_string(loxc_ast_t ast) {
	loxc_string_builder_t builder;
	loxc_string_builder_init(&builder);
	ast_to_string_helper(&builder, ast);
	return loxc_string_builder_complete(&builder);
}
