
#include <stdio.h>
#include <ctype.h>

#include "lex.h"
#include "string.h"
#include "mem.h"

struct loxc_source {
	const char *filename;
	loxc_string_t code;
	size_t *line_start;
	unsigned int n_lines, cap_lines;
};

loxc_source_t loxc_source_new(const char *filename, loxc_string_t code) {
	loxc_source_t source = mem_new(struct loxc_source);
	if (source != NULL) {
		source->filename = filename;
		source->code = code;
		source->n_lines = 1;
		source->cap_lines = 0x16;
		source->line_start = mem_new_array(size_t, source->cap_lines);
		if (source->line_start == NULL) {
			mem_free(source);
			return NULL;
		}
		source->line_start[0] = 0; // first line always starts at 0
	}
	return source;
}

void loxc_source_free(loxc_source_t source) {
	mem_free(source->line_start);
	mem_free(source);
}

const char *loxc_source_filename(loxc_source_t source) {
	return source->filename;
}

loxc_string_t loxc_source_code(loxc_source_t source) {
	return source->code;
}

loxc_string_t loxc_source_line(loxc_source_t source, unsigned int line) {
	if (line > source->n_lines) return loxc_string_new("", 0);

	size_t start = source->line_start[line - 1];
	size_t end;
	if (line == source->n_lines) {
		const char *endp = memchr(source->code.chars + start, '\n',
		  source->code.length - start);
		if (endp == NULL) end = source->code.length;
		else end = (endp - source->code.chars) + 1;
	} else end = source->line_start[line];

	return loxc_string_range(source->code, start, end);
}

static bool loxc_source_append_line(loxc_source_t source, size_t start) {
	if (source->n_lines == source->cap_lines) {
		size_t new_cap = 2 * source->cap_lines;

		size_t *new = mem_resize_array(source->line_start, new_cap);
		if (new == NULL) return false;
		source->cap_lines = new_cap;
		source->line_start = new;
	}

	source->line_start[source->n_lines++] = start;
	return true;
}

struct loxc_lexer {
	loxc_logger_t logger;
	loxc_source_t source;
	loxc_position_t position;
	size_t current;
	bool ok;
};

static double number(loxc_lexer_t);
static loxc_string_t identifier(loxc_lexer_t);
static loxc_token_type_t identifier_type(loxc_string_t identifer);

static bool is_digit(int ch);
static bool is_id(int ch);

static void skip_whitespace(loxc_lexer_t);
static loxc_token_type_t peek_eq(loxc_lexer_t,
  loxc_token_type_t, loxc_token_type_t);
static int peek(loxc_lexer_t);
static int peek_next(loxc_lexer_t);
static int advance(loxc_lexer_t);

loxc_lexer_t loxc_lexer_new(loxc_source_t source, loxc_logger_t logger) {
	loxc_lexer_t lexer = mem_new(struct loxc_lexer);
	if (lexer == NULL) return NULL;

	lexer->logger = logger;
	lexer->source = source;
	lexer->position.line = 1;
	lexer->position.column = 0;
	lexer->current = 0;
	lexer->ok = true;
	return lexer;
}

bool loxc_lexer_free(loxc_lexer_t lexer) {
	bool ok = lexer->ok;
	mem_free(lexer);
	return ok;
}

loxc_source_t loxc_lexer_source(loxc_lexer_t lexer) {
	return lexer->source;
}

loxc_token_t loxc_lex(loxc_lexer_t lexer) {
error:
	skip_whitespace(lexer);

	loxc_token_t token;
	token.position.start = lexer->position;

	int ch = advance(lexer);
	switch (ch) {
		case EOF: token.type = LOXC_TOKEN_EOF; break;
		case '(': token.type = LOXC_TOKEN_LEFT_PAREN; break;
		case ')': token.type = LOXC_TOKEN_RIGHT_PAREN; break;
		case '{': token.type = LOXC_TOKEN_LEFT_BRACE; break;
		case '}': token.type = LOXC_TOKEN_RIGHT_BRACE; break;
		case '+': token.type = LOXC_TOKEN_PLUS; break;
		case '-': token.type = LOXC_TOKEN_MINUS; break;
		case '*': token.type = LOXC_TOKEN_ASTERISK; break;
		case '/': token.type = LOXC_TOKEN_SOLIDUS; break;
		case '!':
			token.type = peek_eq(lexer,
			  LOXC_TOKEN_EXCLAMATION_EQUAL,
			  LOXC_TOKEN_EXCLAMATION);
			break;
		case ';': token.type = LOXC_TOKEN_SEMICOLON; break;
		case '=':
			token.type = peek_eq(lexer,
			  LOXC_TOKEN_EQUAL_EQUAL, LOXC_TOKEN_EQUAL);
			break;
		case '<':
			token.type = peek_eq(lexer,
			  LOXC_TOKEN_LESS_EQUAL, LOXC_TOKEN_LESS);
			break;
		case '>':
			token.type = peek_eq(lexer,
			  LOXC_TOKEN_GREATER_EQUAL, LOXC_TOKEN_GREATER);
			break;
		default:
			if (is_digit(ch)) {
				token.type = LOXC_TOKEN_NUMBER;
				token.value.number = number(lexer);
				break;
			} else if (is_id(ch)) {
				loxc_string_t id = identifier(lexer);
				loxc_token_type_t type = identifier_type(id);
				token.type = type;
				if (type == LOXC_TOKEN_IDENTIFIER)
				  token.value.string = id;
				else loxc_string_free(id);
				break;
			}

			token.position.end = lexer->position;
			/* TODO: Maybe make it coalesce strings of invalid
			   characters into a single error. */
			loxc_logger_error_position(lexer->logger,
			  lexer->source, token.position,
			  "Unexpected character");
			lexer->ok = false;
			goto error;
	}

	token.position.end = lexer->position;
	return token;
}

static double number(loxc_lexer_t lexer) {
	size_t start = lexer->current - 1;
	while (is_digit(peek(lexer))) advance(lexer);
	if (peek(lexer) == '.' && is_digit(peek_next(lexer))) {
		advance(lexer);
		do advance(lexer); while (is_digit(peek(lexer)));
	}
	size_t len = lexer->current - start;

	char *tmp = mem_new_array(char, len + 1);
	if (tmp == NULL) {
		loxc_logger_out_of_memory(lexer->logger);
		lexer->ok = false;
		return 0.0;
	}
	memcpy(tmp, lexer->source->code.chars + start, len);
	tmp[len] = '\0';
	double val = strtod(tmp, NULL);
	mem_free(tmp);

	return val;
}

static loxc_string_t identifier(loxc_lexer_t lexer) {
	size_t start = lexer->current - 1;
	while (is_id(peek(lexer))) advance(lexer);
	size_t len = lexer->current - start;

	char *id = mem_new_array(char, len);
	memcpy(id, lexer->source->code.chars + start, len);
	return loxc_string_new(id, len);
}

static loxc_token_type_t identifier_type(loxc_string_t identifier) {
	switch (identifier.length) {
		case 2:
			switch (identifier.chars[0]) {
				case 'i':
					if (identifier.chars[1] == 'f')
					  return LOXC_TOKEN_IF;
					break;
				case 'o':
					if (identifier.chars[1] == 'r')
					  return LOXC_TOKEN_OR;
					break;
			}
			break;
		case 3:
			switch (identifier.chars[0]) {
				case 'a':
					if (memcmp("nd", identifier.chars+1, 2)
					  == 0) return LOXC_TOKEN_AND;
					break;
				case 'v':
					if (memcmp("ar", identifier.chars+1, 2)
					  == 0) return LOXC_TOKEN_VAR;
					break;
				case 'n':
					if (memcmp("il", identifier.chars+1, 2)
					  == 0) return LOXC_TOKEN_NIL;
					break;
			}
			break;
		case 4:
			switch (identifier.chars[0]) {
				case 'e':
					if (memcmp("lse", identifier.chars+1,
					  3) == 0) return LOXC_TOKEN_ELSE;
					break;
				case 't':
					if (memcmp("rue", identifier.chars+1,
					  3) == 0) return LOXC_TOKEN_TRUE;
					break;
			}
			break;
		case 5:
			switch (identifier.chars[0]) {
				case 'f':
					if (memcmp("alse", identifier.chars+1,
					  4) == 0) return LOXC_TOKEN_FALSE;
					break;
				case 'p':
					if (memcmp("rint", identifier.chars+1,
					  4) == 0) return LOXC_TOKEN_PRINT;
					break;
			}
			break;
	}
	return LOXC_TOKEN_IDENTIFIER;
}

static bool is_digit(int ch) {
	return '0' <= ch && ch <= '9';
}

static bool is_id(int ch) {
	return isalpha(ch) || is_digit(ch) || ch == '_';
}

static void skip_whitespace(loxc_lexer_t lexer) {
	while (true) {
		int ch = peek(lexer);
		if (isspace(ch)) advance(lexer);
		else if (ch != '/' || peek_next(lexer) != '/') return;
		else do {
			advance(lexer);
			ch = peek(lexer);
		} while (ch != '\n' && ch != EOF);
	}
}

static loxc_token_type_t peek_eq(loxc_lexer_t lexer,
  loxc_token_type_t eq_type, loxc_token_type_t not_eq_type) {
	if (peek(lexer) == '=') {
		advance(lexer);
		return eq_type;
	} else return not_eq_type;
}

static int peek(loxc_lexer_t lexer) {
	loxc_string_t *code = &lexer->source->code;
	if (lexer->current == code->length) return EOF;

	const unsigned char *chars = (const unsigned char *) code->chars;
	return chars[lexer->current];
}

static int peek_next(loxc_lexer_t lexer) {
	loxc_string_t *code = &lexer->source->code;
	size_t current = lexer->current + 1;
	if (current >= code->length) return EOF;

	const unsigned char *chars = (const unsigned char *) code->chars;
	return chars[current];
}

static int advance(loxc_lexer_t lexer) {
	int ch = peek(lexer);
	if (ch == EOF) return EOF;

	lexer->current++;

	if (ch == '\n') {
		lexer->position.line++;
		lexer->position.column = 0;
		if (!loxc_source_append_line(lexer->source, lexer->current)) {
			loxc_logger_out_of_memory(lexer->logger);
			lexer->ok = false;
		}
	} else lexer->position.column++;
	return ch;
}

static const char *token_names[] = {
	[LOXC_TOKEN_LEFT_PAREN] = "`('",
	[LOXC_TOKEN_RIGHT_PAREN] = "`)'",
	[LOXC_TOKEN_LEFT_BRACE] = "`{'",
	[LOXC_TOKEN_RIGHT_BRACE] = "`}'",
	[LOXC_TOKEN_PLUS] = "`+'",
	[LOXC_TOKEN_MINUS] = "`-'",
	[LOXC_TOKEN_ASTERISK] = "`*'",
	[LOXC_TOKEN_SOLIDUS] = "`/'",
	[LOXC_TOKEN_EXCLAMATION] = "`!'",
	[LOXC_TOKEN_EQUAL_EQUAL] = "`=='",
	[LOXC_TOKEN_EXCLAMATION_EQUAL] = "`!='",
	[LOXC_TOKEN_LESS] = "`<'",
	[LOXC_TOKEN_LESS_EQUAL] = "`<='",
	[LOXC_TOKEN_GREATER] = "`>'",
	[LOXC_TOKEN_GREATER_EQUAL] = "`>='",
	[LOXC_TOKEN_SEMICOLON] = "`;'",
	[LOXC_TOKEN_EQUAL] = "`='",
	[LOXC_TOKEN_PRINT] = "keyword `print'",
	[LOXC_TOKEN_VAR] = "keyword `var'",
	[LOXC_TOKEN_IF] = "keyword `if'",
	[LOXC_TOKEN_ELSE] = "keyword `else'",
	[LOXC_TOKEN_AND] = "keyword `and'",
	[LOXC_TOKEN_OR] = "keyword `or'",
	[LOXC_TOKEN_NIL] = "keyword `nil'",
	[LOXC_TOKEN_TRUE] = "keyword `true'",
	[LOXC_TOKEN_FALSE] = "keyword `false'",
	[LOXC_TOKEN_NUMBER] = "number",
	[LOXC_TOKEN_IDENTIFIER] = "identifier",
	[LOXC_TOKEN_EOF] = "EOF",
};

const char *loxc_token_type_name(loxc_token_type_t type) {
	return token_names[type];
}

loxc_string_t loxc_token_data(loxc_token_t token) {
	switch (token.type) {
		case LOXC_TOKEN_NUMBER: {
			double num = token.value.number;
			int len = snprintf(NULL, 0, "%g", num);
			char *out = mem_new_array(char, len + 1);
			if (out == NULL) return loxc_string_new_error();
			snprintf(out, len + 1, "%g", num);
			return loxc_string_new(out, len);
		}
		case LOXC_TOKEN_IDENTIFIER: {
			return loxc_string_copy(token.value.string);
		}
		default: {
			// Allocate 1 instead of zero because 0 is not very
			// well-defined by the C standard.
			char *string = mem_new_array(char, 1);
			if (string == NULL) return loxc_string_new_error();
			return loxc_string_new(string, 0);
		}
	}
}

void loxc_token_free(loxc_token_t token) {
	switch (token.type) {
		case LOXC_TOKEN_IDENTIFIER:
			loxc_string_free(token.value.string);
			break;
		default: break;
	}
}

#undef loxc_span_new
loxc_span_t loxc_span_new(loxc_position_t start, loxc_position_t end) {
	return (loxc_span_t){ start, end };
}

#undef loxc_span_none
loxc_span_t loxc_span_none(void) {
	return (loxc_span_t){ {0, 0}, {0, 0} };
}
