
#ifndef LOXC_STRING_H
#define LOXC_STRING_H

#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <stdbool.h>

typedef struct {
	size_t length;
	char *chars;
} loxc_string_t;

loxc_string_t loxc_string_from_cstring(char *string);
loxc_string_t loxc_string_new(char *chars, size_t length);
loxc_string_t loxc_string_new_error(void);
char *loxc_string_to_cstring(loxc_string_t);
void loxc_string_free(loxc_string_t);
bool loxc_string_error(loxc_string_t);
size_t loxc_string_length(loxc_string_t);
char *loxc_string_chars(loxc_string_t);

bool loxc_string_eq(loxc_string_t, loxc_string_t);
loxc_string_t loxc_string_copy(loxc_string_t);
loxc_string_t loxc_string_concat(loxc_string_t, loxc_string_t);
loxc_string_t loxc_string_append(loxc_string_t, loxc_string_t);
loxc_string_t loxc_string_range(loxc_string_t, size_t start, size_t end);

#define loxc_string_new(STR, LEN) ((loxc_string_t){ LEN, STR })
#define loxc_string_new_error() ((loxc_string_t){ 0, NULL })
#define loxc_string_error(STRING) ((STRING).chars == NULL)
#define loxc_string_length(STRING) (STRING).length
#define loxc_string_chars(STRING) (STRING).chars

typedef struct {
	bool out_of_memory;
	size_t cap;
	loxc_string_t string;
} loxc_string_builder_t;

void loxc_string_builder_init(loxc_string_builder_t *);
void loxc_string_builder_cancel(loxc_string_builder_t *);
loxc_string_t loxc_string_builder_complete(loxc_string_builder_t *);
void loxc_string_builder_append(loxc_string_builder_t *, loxc_string_t string);
void loxc_string_builder_appendf(loxc_string_builder_t *,
  const char *format, ...);
void loxc_string_builder_vappendf(loxc_string_builder_t *,
  const char *format, va_list ap);

#endif
