
#ifndef LOXC_HASHMAP_H
#define LOXC_HASHMAP_H

#include "string.h"

typedef struct loxc_hashmap *loxc_hashmap_t;
typedef struct loxc_hashmap_iter *loxc_hashmap_iter_t;

loxc_hashmap_t loxc_hashmap_new(void);
void loxc_hashmap_free(loxc_hashmap_t);

void *loxc_hashmap_set(loxc_hashmap_t, loxc_string_t key, void *value);
void *loxc_hashmap_get(loxc_hashmap_t, loxc_string_t key);

loxc_hashmap_iter_t loxc_hashmap_iter(loxc_hashmap_t);
loxc_hashmap_iter_t loxc_hashmap_free_iter(loxc_hashmap_t);
void loxc_hashmap_iter_free(loxc_hashmap_iter_t);

loxc_string_t loxc_hashmap_iter_next(loxc_hashmap_iter_t);
void *loxc_hashmap_iter_set(loxc_hashmap_iter_t, void *);
void *loxc_hashmap_iter_get(loxc_hashmap_iter_t);

#endif
