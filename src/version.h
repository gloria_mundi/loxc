
#ifndef LOXC_VERSION_H
#define LOXC_VERSION_H

#include <stdint.h>

/* The compile time version number, as a number in the format:
      <8 bits major version> <8 bits minor version> <8 bits patchlevel>
   (this is the same format used by libcurl, for instance)
 */
#define LOXC_VERSION 0x000100
/* Ditto, but at runtime. Possibly different if libloxc is dynamically
   linked. */
uint_least32_t loxc_version(void);

/* The compile time version number, as a string in the form
      "<major>.<minor>.<patch>"
 */
#define LOXC_VERSION_STRING "0.1.0"
/* Ditto, but at runtime. Possibly different if libloxc is dynamically
   linked. */
const char *loxc_version_string(void);

/* The value of the __DATE__ predefined macro at the time libloxc was compiled
 */
const char *loxc_compilation_date(void);
/* The value of the __TIME__ predefined macro at the time libloxc was compiled
 */
const char *loxc_compilation_time(void);

#endif
