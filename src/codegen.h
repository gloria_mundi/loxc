
#ifndef LOXC_CODEGEN_H
#define LOXC_CODEGEN_H

#include <llvm-c/Core.h>

#include "analysis.h"
#include "ast.h"
#include "lex.h"

LLVMModuleRef loxc_codegen(
  loxc_analysis_t analysis, loxc_ast_t ast, loxc_source_t source);
LLVMModuleRef loxc_codegen_in_context(LLVMContextRef context,
  loxc_analysis_t analysis, loxc_ast_t ast, loxc_source_t source);
void loxc_codegen_to_module(LLVMModuleRef module,
  loxc_analysis_t analysis, loxc_ast_t ast, loxc_source_t source);
void loxc_codegen_to_function(LLVMValueRef function,
  loxc_analysis_t analysis, loxc_ast_t ast, loxc_source_t source);

#endif
