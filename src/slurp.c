
#include <stdio.h>

#include "slurp.h"
#include "string.h"
#include "mem.h"
#include "logger.h"

loxc_string_t loxc_slurp(loxc_logger_t logger, FILE *f, const char *filename) {
	/* There are two ways to slurp a file (read its entire contents into a
	   string):
	   1. The seek trick: Seek to the end of the file, use ftell() to get
	      the length, seek back to the beginning. (This approach is used by
	      clox.)
	   2. Incrementally read the file into a growing buffer.
	   The problem with 1 is that the C standard guarantees just about
	   nothing for seeking and telling on text streams. So we use 2
	   instead.
	 */

	size_t capacity = 0x40, length = 0;
	char *buffer = NULL;

	while (!feof(f)) {
		if (ferror(f)) {
			loxc_logger_error(logger,
			  "Error while reading from `%s'", filename);
			mem_free(buffer);
			return loxc_string_new_error();
		}

		char *new_buffer = mem_resize_array(buffer, capacity);
		if (new_buffer == NULL) {
			loxc_logger_out_of_memory(logger);
			mem_free(buffer);
			return loxc_string_new_error();
		}
		buffer = new_buffer;

		length += fread(buffer + length, 1, capacity - length, f);
		capacity *= 2;
	}

	return loxc_string_new(buffer, length);
}
