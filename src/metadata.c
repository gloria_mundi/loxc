
#include <stddef.h>
#include <stdint.h>

#include "metadata.h"
#include "mem.h"

typedef struct entry {
	loxc_metadata_key_t key;
	void *value;
} *entry_t;

typedef uint_least32_t hash_t;

struct loxc_metadata {
	size_t len, cap;
	entry_t entries;
};

static entry_t find(loxc_metadata_t, loxc_metadata_key_t key);
static hash_t hash_key(loxc_metadata_key_t key);

loxc_metadata_t loxc_metadata_new(void) {
	loxc_metadata_t metadata = mem_new(struct loxc_metadata);
	metadata->len = metadata->cap = 0;
	metadata->entries = NULL;
	return metadata;
}

void loxc_metadata_free(loxc_metadata_t metadata) {
	if (metadata->entries != NULL) {
		for (size_t i = 0; i <= metadata->cap; i++) {
			entry_t entry = &metadata->entries[i];
			if (entry->key != NULL) (*entry->key)(entry->value);
		}
		mem_free(metadata->entries);
	}

	mem_free(metadata);
}

void *loxc_metadata_set(loxc_metadata_t metadata,
  loxc_metadata_key_t key, void *value) {
	if (metadata->len * 3 >= metadata->cap * 2) {
		size_t new_cap, old_cap = metadata->cap;
		entry_t old = metadata->entries;

		if (old_cap == 0) new_cap = 0x10;
		else new_cap = 2 * ++old_cap;

		entry_t new = mem_new_array(struct entry, new_cap);
		for (size_t i = 0; i < new_cap; i++) new[i].key = NULL;

		metadata->cap = new_cap - 1;
		metadata->entries = new;

		for (size_t i = 0; i < old_cap; i++)
		  loxc_metadata_set(metadata, old[i].key, old[i].value);
		mem_free(old);
	}

	entry_t entry = find(metadata, key);
	if (entry->key == NULL) {
		metadata->len++;
		entry->key = key;
		entry->value = value;
		return NULL;
	} else {
		void *old = entry->value;
		entry->value = value;
		return old;
	}
}

void *loxc_metadata_get(loxc_metadata_t metadata, loxc_metadata_key_t key) {
	if (metadata->entries == NULL) return NULL;

	entry_t entry = find(metadata, key);
	if (entry->key == NULL) return NULL;
	else return entry->value;
}

static entry_t find(loxc_metadata_t metadata, loxc_metadata_key_t key) {
	for (size_t pos = hash_key(key) & metadata->cap;;
	  pos = (pos+1) & metadata->cap) {
		entry_t entry = &metadata->entries[pos];
		if (entry->key == NULL || entry->key == key) return entry;
	}
}

static hash_t hash_key(loxc_metadata_key_t key) {
	hash_t hash = (hash_t)
#if UINTPTR_MAX >= 0xFFFFFFFF
/* Silence -Wpointer-to-int-cast, but only if this won't lose information */
	                       (uintptr_t)
#endif
	                                   key;
	hash = UINT32_C(0x55555555) * (hash ^ (hash >> 16));
	hash = UINT32_C(2654435761) * (hash ^ (hash >> 16));
	return hash;
}
