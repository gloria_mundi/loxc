
#include <assert.h>

#include "analysis.h"
#include "ast.h"
#include "type.h"
#include "lex.h"
#include "logger.h"
#include "hashmap.h"
#include "mem.h"
#include "macros.h"

typedef struct mutation *mutation_t;

typedef struct variable {
	int index;
	loxc_typeset_t type;
	mutation_t mutation;
} *variable_t;

typedef struct global {
	struct variable variable;
	bool declared;
} *global_t;

typedef struct local {
	struct variable variable;
	loxc_span_t position;
	bool initialised;
} *local_t;

typedef struct variable_list {
	size_t len;
	struct loxc_analysis_varinfo vars[];
} *variable_list_t;

typedef struct variable_list_builder {
	size_t cap;
	variable_list_t list;
} variable_list_builder_t;

typedef struct scope {
	struct scope *parent;
	loxc_hashmap_t locals_by_name;
	variable_list_builder_t locals;
} *scope_t;

struct mutation {
	variable_t variable;
	loxc_typeset_t type;
	unsigned int nesting_level;
	int scope;
	mutation_t next, parent;
};

typedef struct mutation_list {
	size_t len;
	struct loxc_analysis_mutation mutations[];
} *mutation_list_t;

struct loxc_analysis {
	char dummy;
};

typedef struct analyser {
	loxc_analysis_t analysis;
	loxc_source_t source;
	loxc_logger_t logger;

	loxc_hashmap_t globals_by_name;
	variable_list_t globals;
	scope_t scope;
	int scope_number;

	mutation_t mutations;
	unsigned int nesting_level;

	bool ok;
} *analyser_t;

static void analyse_ast(analyser_t, loxc_ast_t);
static void analyse_stmt(analyser_t, loxc_stmt_t);
static void analyse_if(analyser_t, loxc_stmt_if_t);
static loxc_typeset_t analyse_expr(analyser_t, loxc_expr_t);

static local_t declare_local(analyser_t analyser,
  loxc_span_t position, loxc_string_t name);
static loxc_typeset_t use_variable(analyser_t analyser,
  loxc_expr_t expr, loxc_string_t name, loxc_typeset_t new_type_or_null);
static void mutate(analyser_t analyser, int scope, variable_t variable,
  loxc_analysis_varinfo_t info, loxc_typeset_t type);

static mutation_t enter(analyser_t analyser);
static void leave(analyser_t analyser, mutation_t save,
  loxc_metadata_t metadata);

static void variable_list_builder_init(variable_list_builder_t *);
static int variable_list_builder_append(variable_list_builder_t *,
  struct loxc_analysis_varinfo);
static variable_list_t variable_list_build(variable_list_builder_t *);

static void set_variable_list(loxc_ast_t, variable_list_t);
static void set_variable(loxc_metadata_t metadata, int scope, int index);
static void set_mutation_list(loxc_metadata_t metadata, mutation_list_t list);
static void set_type(loxc_expr_t, loxc_typeset_t type);

static variable_list_t get_variable_list(loxc_ast_t ast);
static loxc_analysis_variable_t get_variable(loxc_metadata_t metadata);
static mutation_list_t get_mutation_list(loxc_metadata_t metadata);

static void MD_VARIABLE_LIST(void *);
static void MD_VARIABLE_INFO(void *);
static void MD_MUTATION_LIST(void *);
static void MD_TYPE(void *);

loxc_analysis_t loxc_analyse(loxc_ast_t ast, loxc_source_t source,
  loxc_logger_t logger) {
	struct analyser analyser;
	analyser.analysis = mem_new(struct loxc_analysis);
	analyser.source = source;
	analyser.logger = logger;
	analyser.ok = true;

	analyser.globals_by_name = loxc_hashmap_new();
	variable_list_builder_t globals;
	variable_list_builder_init(&globals);

	size_t n_stmts = loxc_ast_count_stmts(ast);
	for (size_t i = 0; i < n_stmts; i++) {
		loxc_stmt_t stmt = loxc_ast_get_stmt(ast, i);
		if (stmt->type != LOXC_STMT_DECLARE) continue;

		loxc_stmt_declare_t declare = (loxc_stmt_declare_t) stmt;
		loxc_string_t name = declare->name;
		if (loxc_hashmap_get(analyser.globals_by_name, name) != NULL) {
			/* Redeclarations of global variables are allowed. */
			continue;
		}

		int index = variable_list_builder_append(&globals,
		  (struct loxc_analysis_varinfo)
		    { name, loxc_typeset_empty() });
		global_t global = mem_new(struct global);
		global->variable.index = index;
		global->variable.mutation = NULL;
		global->declared = false;
		loxc_hashmap_set(analyser.globals_by_name, name, global);
	}

	analyser.globals = variable_list_build(&globals);

	analyser.scope = NULL;
	analyser.scope_number = 0;

	analyser.mutations = NULL;
	analyser.nesting_level = 0;

	for (size_t i = 0; i < n_stmts; i++) {
		loxc_stmt_t stmt = loxc_ast_get_stmt(ast, i);
		if (stmt->type == LOXC_STMT_DECLARE) {
			mutation_t save = enter(&analyser);
			loxc_stmt_declare_t declare =
			  (loxc_stmt_declare_t) stmt;
			loxc_typeset_t type =
			  analyse_expr(&analyser, declare->value);
			global_t global =
			  loxc_hashmap_get(analyser.globals_by_name,
			  declare->name);
			assert(global != NULL);
			global->declared = true;
			set_variable(stmt->metadata,
			  -1, global->variable.index);
			/* We don't need to worry about type's reference count
			   here, because analyse_expr() returns a borrowed type
			   and mutate() accepts a borrowed type.             */
			mutate(&analyser, -1, &global->variable,
			  &analyser.globals->vars[global->variable.index],
			  type);
			leave(&analyser, save, stmt->metadata);
		} else analyse_stmt(&analyser, stmt);
	}

	mutation_t mutation = analyser.mutations;
	while (mutation != NULL) {
		mutation_t next = mutation->next;
		assert(mutation->scope == -1);
		assert(mutation->parent == NULL);
		mem_free(mutation);
		mutation = next;
	}

	set_variable_list(ast, analyser.globals);

	loxc_hashmap_iter_t iter =
	  loxc_hashmap_free_iter(analyser.globals_by_name);
	while (!loxc_string_error(loxc_hashmap_iter_next(iter))) {
		global_t global = loxc_hashmap_iter_get(iter);
		loxc_typeset_unref(global->variable.type);
		mem_free(global);
	}
	loxc_hashmap_iter_free(iter);

	if (analyser.ok) return analyser.analysis;
	else {
		loxc_analysis_free(analyser.analysis);
		return NULL;
	}
}

void loxc_analysis_free(loxc_analysis_t analysis) {
	mem_free(analysis);
}

static void analyse_ast(analyser_t analyser, loxc_ast_t ast) {
	scope_t scope = mem_new(struct scope);
	scope->parent = analyser->scope;
	scope->locals_by_name = loxc_hashmap_new();
	variable_list_builder_init(&scope->locals);
	analyser->scope = scope;
	analyser->scope_number++;

	size_t n_stmts = loxc_ast_count_stmts(ast);
	for (size_t i = 0; i < n_stmts; i++)
	  analyse_stmt(analyser, loxc_ast_get_stmt(ast, i));

	analyser->scope = scope->parent;
	analyser->scope_number--;

	set_variable_list(ast, variable_list_build(&scope->locals));

	loxc_hashmap_iter_t iter =
	  loxc_hashmap_free_iter(scope->locals_by_name);
	while (!loxc_string_error(loxc_hashmap_iter_next(iter))) {
		local_t local = loxc_hashmap_iter_get(iter);
		loxc_typeset_unref(local->variable.type);
		mem_free(local);
	}
	loxc_hashmap_iter_free(iter);

	mem_free(scope);
}

static void analyse_stmt(analyser_t analyser, loxc_stmt_t stmt) {
	mutation_t save = enter(analyser);
	switch (stmt->type) {
		case LOXC_STMT_NULL: break;
		case LOXC_STMT_EXPR: {
			loxc_stmt_expr_t expr = (loxc_stmt_expr_t) stmt;
			analyse_expr(analyser, expr->expr);
			break;
		}
		case LOXC_STMT_PRINT: {
			loxc_stmt_print_t print = (loxc_stmt_print_t) stmt;
			analyse_expr(analyser, print->expr);
			break;
		}
		case LOXC_STMT_DECLARE: {
			/* Declarations at global scope are handled
			   by loxc_analyse()                                */
			assert(analyser->scope != NULL);

			loxc_stmt_declare_t declare =
			  (loxc_stmt_declare_t) stmt;
			local_t local = declare_local(analyser,
			  stmt->position, declare->name);
			loxc_typeset_t type =
			  analyse_expr(analyser, declare->value);
			set_variable(stmt->metadata, 0, local->variable.index);
			mutate(analyser, 0, &local->variable,
			  &analyser->scope->locals.list->vars[
			                         local->variable.index],
			  type);
			local->initialised = true;
			break;
		}
		case LOXC_STMT_BLOCK: {
			loxc_stmt_block_t block = (loxc_stmt_block_t) stmt;
			analyse_ast(analyser, block->inner);
			break;
		}
		case LOXC_STMT_IF: {
			loxc_stmt_if_t if_ = (loxc_stmt_if_t) stmt;
			analyse_if(analyser, if_);
			break;
		}
	}
	leave(analyser, save, stmt->metadata);
}

static void analyse_if(analyser_t analyser, loxc_stmt_if_t if_) {
	analyse_expr(analyser, if_->condition);

	mutation_t save_full = enter(analyser);
	analyse_stmt(analyser, if_->if_);
	for (mutation_t current = analyser->mutations; current != NULL;
	  current = current->next) {
		loxc_typeset_t old_type = current->type;
		current->type = current->variable->type;
		current->variable->type = old_type;
	}

	mutation_t save_inner = enter(analyser);
	analyse_stmt(analyser, if_->else_);
	analyser->nesting_level--;

	mutation_list_t if_mutations = get_mutation_list(if_->if_->metadata);
	size_t if_count = if_mutations == NULL ? 0 : if_mutations->len;

	mutation_list_t else_mutations =
	  get_mutation_list(if_->else_->metadata);
#ifndef NDEBUG
	size_t else_count = else_mutations == NULL ? 0 : else_mutations->len;
#endif

	size_t i = 0, count_new = 0;
	for (mutation_t current = analyser->mutations; current != NULL;
	  current = current->next, i++) {
		mutation_t merge_with = current->parent;
		if (merge_with == NULL ||
		  merge_with->nesting_level != analyser->nesting_level) {
			count_new++;
			merge_with = current;
		}

		loxc_typeset_t merged =
		  loxc_typeset_union(current->variable->type,
		    merge_with->type);
		loxc_typeset_unref(current->variable->type);
		current->variable->type = merged;

		loxc_typeset_ref(merged);
		else_mutations->mutations[i].force_type = merged;
	}
	assert(i == else_count);

	size_t all_count = if_count + count_new;

	if_mutations = mem_resize_with_array(if_mutations,
	  struct loxc_analysis_mutation, all_count);
	if_mutations->len = all_count;
	set_mutation_list(if_->if_->metadata, if_mutations);

	else_mutations = mem_resize_with_array(else_mutations,
	  struct loxc_analysis_mutation, if_mutations->len);
	else_mutations->len = if_mutations->len;
	set_mutation_list(if_->else_->metadata, else_mutations);

	size_t j = 0;
	mutation_t *current_ptr = &save_inner;
	for (mutation_t current; (current = *current_ptr) != NULL; j++) {
		loxc_typeset_t if_type = current->type;
		loxc_typeset_t var_type = current->variable->type;
		loxc_typeset_t new_type;
		if (current->variable->mutation == current) {
			/* This variable has not been mutated by the else
			   clause.  */
			new_type = loxc_typeset_union(if_type, var_type);
			current->variable->type = new_type;
			current->type = var_type;

			loxc_typeset_ref(var_type);
			loxc_typeset_ref(new_type);
			else_mutations->mutations[i++] =
			  (struct loxc_analysis_mutation) {
				.variable =
				  if_mutations->mutations[j].variable,
				.type = var_type,
				.force_type = new_type,
			};

			current_ptr = &current->next;
		} else {
			// This variable has been mutated by the else clause.
			new_type = var_type;

			*current_ptr = current->next;
			mem_free(current);
		}

		loxc_typeset_ref(new_type);
		if_mutations->mutations[j].force_type = new_type;
		loxc_typeset_unref(if_type);
	}
	assert(i == else_mutations->len);
	assert(j == if_count);
	i = 0;

	*current_ptr = analyser->mutations;
	for (mutation_t current = analyser->mutations; current != NULL;
	  current = current->next, i++) {
		if (current->parent != NULL &&
		  current->parent->nesting_level == analyser->nesting_level) {
			continue;
		}

		loxc_typeset_t old_type = current->type;
		loxc_typeset_t new_type = current->variable->type;
		loxc_typeset_ref(old_type);
		loxc_typeset_ref(new_type);
		if_mutations->mutations[j++] =
		  (struct loxc_analysis_mutation) {
			.variable = else_mutations->mutations[i].variable,
			.type = old_type,
			.force_type = new_type,
		};
	}
	assert(i == else_count);
	assert(j == if_mutations->len);

	analyser->mutations = save_inner;
	leave(analyser, save_full, NULL);
}

static loxc_typeset_t analyse_expr(analyser_t analyser, loxc_expr_t expr) {
	mutation_t save = enter(analyser);
	loxc_typeset_t type;
	switch (expr->type) {
		case LOXC_EXPR_NUMBER:
			type = loxc_typeset_primitive(LOXC_PRIMITIVE_NUMBER);
			break;
		case LOXC_EXPR_BOOL:
			type = loxc_typeset_primitive(LOXC_PRIMITIVE_BOOL);
			break;
		case LOXC_EXPR_NIL:
			type = loxc_typeset_primitive(LOXC_PRIMITIVE_NIL);
			break;
		case LOXC_EXPR_BINARY: {
			loxc_expr_binary_t binary = (loxc_expr_binary_t) expr;
			loxc_typeset_t left =
			  analyse_expr(analyser, binary->left);
			loxc_typeset_t right =
			  analyse_expr(analyser, binary->right);

			if (binary->op == LOXC_EXPR_EQ ||
			    binary->op == LOXC_EXPR_NE) {
				type =
				  loxc_typeset_primitive(LOXC_PRIMITIVE_BOOL);
				break;
			}

			if (binary->op == LOXC_EXPR_AND ||
			    binary->op == LOXC_EXPR_OR) {
				if (binary->op == LOXC_EXPR_AND)
				  left = loxc_typeset_limit_falsy(left);
				else left = loxc_typeset_limit_truthy(left);
				type = loxc_typeset_union(left, right);
				loxc_typeset_unref(left);
				break;
			}

			loxc_primitive_set_t
			  left_prim  = loxc_typeset_primitive_set(left),
			  right_prim = loxc_typeset_primitive_set(right);
			if (left_prim & LOXC_PRIMITIVE_SET_NUMBER &&
			  right_prim & LOXC_PRIMITIVE_SET_NUMBER) {
				type = loxc_typeset_primitive(
				  binary->op < LOXC_EXPR_ARITH ?
				    LOXC_PRIMITIVE_NUMBER :
				    LOXC_PRIMITIVE_BOOL);
			} else type = loxc_typeset_empty();
			break;
		}
		case LOXC_EXPR_UNARY: {
			loxc_expr_unary_t unary = (loxc_expr_unary_t) expr;
			loxc_typeset_t arg =
			  analyse_expr(analyser, unary->arg);
			loxc_primitive_set_t prim =
			  loxc_typeset_primitive_set(arg);
			if (unary->op == LOXC_EXPR_NOT)
			  type = loxc_typeset_primitive(LOXC_PRIMITIVE_BOOL);
			else if (prim & LOXC_PRIMITIVE_SET_NUMBER)
			  type = loxc_typeset_primitive(LOXC_PRIMITIVE_NUMBER);
			else type = loxc_typeset_empty();
			break;
		}
		case LOXC_EXPR_VARIABLE: {
			loxc_expr_variable_t variable =
			  (loxc_expr_variable_t) expr;
			type =
			  use_variable(analyser, expr, variable->name, NULL);
			break;
		}
		case LOXC_EXPR_ASSIGN: {
			loxc_expr_assign_t assign = (loxc_expr_assign_t) expr;
			type = use_variable(analyser, expr, assign->name,
			  analyse_expr(analyser, assign->value));
			break;
		}
		default: unreachable();
	}
	leave(analyser, save, expr->metadata);
	set_type(expr, type);
	return type;
}

static local_t declare_local(analyser_t analyser,
  loxc_span_t position, loxc_string_t name) {
	scope_t scope = analyser->scope;

	local_t local = mem_new(struct local);
	local->variable.index = variable_list_builder_append(&scope->locals,
	  (struct loxc_analysis_varinfo){ name, loxc_typeset_empty() });
	local->variable.type = NULL;
	local->variable.mutation = NULL;
	local->position = position;
	local->initialised = false;
	local_t prev = loxc_hashmap_set(scope->locals_by_name, name, local);
	if (prev == NULL) return local;

	/* Redeclaration at local scope is an error */
	analyser->ok = false;
	char *cname = loxc_string_to_cstring(name);
	loxc_logger_error_position(analyser->logger, analyser->source,
	  position, "Redeclaration of variable `%s'", cname);
	mem_free(cname);
	loxc_logger_error_position(analyser->logger, analyser->source,
	  prev->position, "Initial declaration was here");

	loxc_hashmap_set(scope->locals_by_name, name, prev);
	mem_free(local);
	/* Mark it as uninitialised to catch uses in the initialisation, even
	   if it's a redeclaration.                                          */
	prev->initialised = false;
	return prev;
}

static loxc_typeset_t use_variable(analyser_t analyser,
  loxc_expr_t expr, loxc_string_t variable, loxc_typeset_t type) {
	scope_t scope = analyser->scope;

	for (int scope_pos = 0; scope != NULL;
	  scope = scope->parent, scope_pos++) {
		local_t local =
		  loxc_hashmap_get(scope->locals_by_name, variable);
		if (local == NULL) continue;

		/* We found a local variable of the same name. */
		if (!local->initialised) {
			analyser->ok = false;
			char *cname = loxc_string_to_cstring(variable);
			loxc_logger_error_position(analyser->logger,
			  analyser->source, expr->position,
			  "Cannot access variable `%s' in initialiser", cname);
			mem_free(cname);
			return loxc_typeset_empty();
		}

		set_variable(expr->metadata, scope_pos, local->variable.index);
		if (type == NULL) type = local->variable.type;
		else mutate(analyser, scope_pos, &local->variable,
		  &scope->locals.list->vars[local->variable.index], type);
		loxc_typeset_ref(type);
		return type;
	}

	global_t global =
	  loxc_hashmap_get(analyser->globals_by_name, variable);
	if (global == NULL || !global->declared) {
		/* Using an undeclared variable is not a compile time error
		   in the interpreters, so it's not a compile time error
		   here either. But we produce a warning, because this is most
		   certainly a bug.                                        */
		char *cname = loxc_string_to_cstring(variable);
		loxc_logger_error_position(analyser->logger, analyser->source,
		  expr->position,
		  "Warning: Variable `%s' not declared", cname);
		mem_free(cname);

		set_variable(expr->metadata, -1, -1);
		return loxc_typeset_empty();
	}

	set_variable(expr->metadata, -1, global->variable.index);
	if (type == NULL) type = global->variable.type;
	else mutate(analyser, -1, &global->variable,
	  &analyser->globals->vars[global->variable.index], type);
	loxc_typeset_ref(type);
	return type;
}

static void mutate(analyser_t analyser, int scope, variable_t variable,
  loxc_analysis_varinfo_t info, loxc_typeset_t new_type) {
	loxc_typeset_ref(new_type);

	loxc_typeset_t old_union = info->type_union;
	info->type_union = loxc_typeset_union(old_union, new_type),
	loxc_typeset_unref(old_union);

	mutation_t old = variable->mutation;
	if (old != NULL && analyser->nesting_level == old->nesting_level) {
		loxc_typeset_unref(variable->type);
		variable->type = new_type;
		return;
	}

	mutation_t mutation = mem_new(struct mutation);
	mutation->variable = variable;
	mutation->type = variable->type;
	mutation->nesting_level = analyser->nesting_level;
	mutation->scope = scope == -1 ? -1 : analyser->scope_number - scope;
	mutation->next = analyser->mutations;
	mutation->parent = old;
	variable->type = new_type;
	variable->mutation = mutation;
	analyser->mutations = mutation;
}

static mutation_t enter(analyser_t analyser) {
	mutation_t save = analyser->mutations;
	analyser->mutations = NULL;
	analyser->nesting_level++;
	return save;
}

static void leave(analyser_t analyser, mutation_t save,
  loxc_metadata_t metadata) {
	unsigned int nesting_level = --analyser->nesting_level;
	int scope_number = analyser->scope_number;

	/* We do two passes over the list of mutations:
	   - In the first pass, we identify and remove mutations of variables
	     which have gone out of scope, and count the number of remaining
	     mutations.
	   - In the second pass, we collect the mutations into an array and
	     update mutations whose parents have become relevant again.
	 */
	size_t n_mutated = 0;
	for (mutation_t current, *current_ptr = &analyser->mutations;
	  (current = *current_ptr) != NULL;) {
		if (current->scope > scope_number) {
			/* The mutated variable has gone out of scope.
			   In this case, we can assume that the mutation has
			   no parents.                                       */
			*current_ptr = current->next;
			assert(current->parent == NULL);
			/* We don't need to unref the old type here, because
			   it is the initial type, assigned at declaration
			   time (i.e. no type / NULL).                      */
			mem_free(current);
		} else {
			n_mutated++;
			current_ptr = &current->next;
		}
	}

	mutation_list_t array;
	if (metadata != NULL && n_mutated != 0) {
		array = mem_new_with_array(struct mutation_list,
		  struct loxc_analysis_mutation, n_mutated);
		array->len = n_mutated;
	} else array = NULL;

	size_t i = 0;
	mutation_t *current_ptr = &analyser->mutations;
	for (mutation_t current; (current = *current_ptr) != NULL; i++) {
		if (metadata != NULL) {
			loxc_typeset_t type = current->variable->type;
			loxc_typeset_ref(type);
			array->mutations[i] =
			  (struct loxc_analysis_mutation) {
				.variable.scope = current->scope == -1 ? -1 :
				                 scope_number - current->scope,
				.variable.index = current->variable->index,
				.type = type,
				.force_type = NULL,
			};
		}

		mutation_t parent = current->parent;
		if (parent != NULL && parent->nesting_level == nesting_level) {
			*current_ptr = current->next;
			loxc_typeset_unref(current->type);
			mem_free(current);
			parent->variable->mutation = parent;
		} else {
			current->nesting_level--;
			assert(current->nesting_level == nesting_level);
			current_ptr = &current->next;
		}
	}
	assert(i == n_mutated);
	*current_ptr = save;

	if (array != NULL) set_mutation_list(metadata, array);
}

static void variable_list_builder_init(variable_list_builder_t *builder) {
	builder->cap = 0;
	builder->list = NULL;
}

static int variable_list_builder_append(variable_list_builder_t *builder,
  struct loxc_analysis_varinfo info) {
	if (builder->list == NULL) {
		builder->cap = 0x10;
		builder->list = mem_new_with_array(struct variable_list,
		  loxc_string_t, builder->cap);
		builder->list->len = 0;
	} else if (builder->cap <= builder->list->len) {
		builder->cap *= 2;
		builder->list = mem_resize_with_array(builder->list,
		  loxc_string_t, builder->cap);
	}

	variable_list_t list = builder->list;
	int pos = list->len++;
	list->vars[pos] = info;
	return pos;
}

static variable_list_t variable_list_build(variable_list_builder_t *builder) {
	return builder->list;
}

static void set_variable_list(loxc_ast_t ast, variable_list_t list) {
	if (list != NULL)
	  loxc_metadata_set(loxc_ast_metadata(ast), MD_VARIABLE_LIST, list);
}

static void set_variable(loxc_metadata_t metadata, int scope, int index) {
	loxc_analysis_variable_t var = mem_new(struct loxc_analysis_variable);
	var->scope = scope;
	var->index = index;
	loxc_metadata_set(metadata, MD_VARIABLE_INFO, var);
}

static inline void set_mutation_list(loxc_metadata_t metadata,
  mutation_list_t list) {
	loxc_metadata_set(metadata, MD_MUTATION_LIST, list);
}

static inline void set_type(loxc_expr_t expr, loxc_typeset_t type) {
	loxc_metadata_set(expr->metadata, MD_TYPE, type);
}

/*** Accessors ***/
size_t loxc_analysis_count_variables(loxc_ast_t ast) {
	variable_list_t list = get_variable_list(ast);
	if (list == NULL) return 0;
	else return list->len;
}

loxc_analysis_varinfo_t loxc_analysis_varinfo(loxc_ast_t ast, size_t index) {
	variable_list_t list = get_variable_list(ast);
	assert(list != NULL && index < list->len);
	return &list->vars[index];
}

#define GET_VARIABLE(T) \
	loxc_analysis_variable_t \
	  loxc_analysis_ ## T ## _variable(loxc_ ## T ## _t arg) { \
		loxc_analysis_variable_t info = get_variable(arg->metadata); \
		assert(info != NULL); \
		return info; \
	}

GET_VARIABLE(stmt) GET_VARIABLE(expr)

#define GET_MUTATION(T) \
	size_t loxc_analysis_ ## T ## _count_mutations( \
	  loxc_ ## T ## _t arg) { \
		mutation_list_t list = get_mutation_list(arg->metadata); \
		if (list == NULL) return 0; \
		else return list->len; \
	} \
	loxc_analysis_mutation_t loxc_analysis_ ## T ## _mutation( \
	  loxc_ ## T ## _t arg, size_t index) { \
		mutation_list_t list = get_mutation_list(arg->metadata); \
		assert(list != NULL && index < list->len); \
		return &list->mutations[index]; \
	}

GET_MUTATION(stmt) GET_MUTATION(expr)

loxc_typeset_t loxc_analysis_expr_type(loxc_expr_t expr) {
	return loxc_metadata_get(expr->metadata, MD_TYPE);
}

static inline variable_list_t get_variable_list(loxc_ast_t ast) {
	return loxc_metadata_get(loxc_ast_metadata(ast), MD_VARIABLE_LIST);
}

static inline loxc_analysis_variable_t get_variable(loxc_metadata_t metadata) {
	return loxc_metadata_get(metadata, MD_VARIABLE_INFO);
}

static inline mutation_list_t get_mutation_list(loxc_metadata_t metadata) {
	return loxc_metadata_get(metadata, MD_MUTATION_LIST);
}

/*** Metadata ***/
static void MD_VARIABLE_LIST(void *info) {
	variable_list_t list = info;
	for (size_t i = 0; i < list->len; i++) {
		// The variable name string is owned by the declaring
		// statement node, so it would be wrong to free it here.
		loxc_typeset_unref(list->vars[i].type_union);
	}

	mem_free(list);
}

static void MD_VARIABLE_INFO(void *info) {
	mem_free(info);
}

static void MD_MUTATION_LIST(void *info) {
	mutation_list_t mutations = info;
	for (size_t i = 0; i < mutations->len; i++) {
		loxc_typeset_unref(mutations->mutations[i].type);

		loxc_typeset_t force = mutations->mutations[i].force_type;
		if (force != NULL) loxc_typeset_unref(force);
	}

	mem_free(info);
}

static void MD_TYPE(void *info) {
	loxc_typeset_unref(info);
}
