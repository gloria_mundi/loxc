
#include <llvm-c/Core.h>
#include <llvm-c/Target.h>
#include <stdbool.h>
#include <assert.h>

#include "codegen.h"
#include "ast.h"
#include "lex.h"
#include "analysis.h"
#include "logger.h"
#include "macros.h"
#include "mem.h"
#include "hashmap.h"

LLVMModuleRef loxc_codegen(
  loxc_analysis_t analysis, loxc_ast_t ast, loxc_source_t source) {
	return loxc_codegen_in_context(LLVMGetGlobalContext(),
	  analysis, ast, source);
}

LLVMModuleRef loxc_codegen_in_context(LLVMContextRef context,
  loxc_analysis_t analysis, loxc_ast_t ast, loxc_source_t source) {
	LLVMModuleRef module = LLVMModuleCreateWithNameInContext(
	  loxc_source_filename(source),
	  context
	);
	loxc_codegen_to_module(module, analysis, ast, source);
	return module;
}

void loxc_codegen_to_module(LLVMModuleRef module,
  loxc_analysis_t analysis, loxc_ast_t ast, loxc_source_t source) {
	LLVMValueRef main = LLVMAddFunction(module, "lox::main",
	  LLVMFunctionType(LLVMVoidTypeInContext(LLVMGetModuleContext(module)),
	    NULL, 0, false));
	LLVMSetFunctionCallConv(main, LLVMFastCallConv);
	loxc_codegen_to_function(main, analysis, ast, source);
}

typedef struct scope {
	struct scope *parent;

#ifndef NDEBUG
	size_t n_locals;
#endif
	LLVMValueRef locals[];
} *scope_t;

struct global {
	loxc_typeset_t ty;
	LLVMValueRef var;
};

typedef struct codegen {
	LLVMContextRef context;
	LLVMModuleRef module;
	loxc_source_t source;
	LLVMValueRef print_functions[LOXC_N_PRIMITIVES], panic_function;
	bool reachable;

#ifndef NDEBUG
	size_t n_globals;
#endif
	struct global *globals;
	scope_t scope;
} *codegen_t;

static void codegen_scope(codegen_t, LLVMBuilderRef, loxc_ast_t);
static void codegen_ast(codegen_t, LLVMBuilderRef, loxc_ast_t);
static void codegen_stmt(codegen_t, LLVMBuilderRef, loxc_stmt_t);
static LLVMValueRef codegen_expr(codegen_t, LLVMBuilderRef, loxc_expr_t);

static void print(codegen_t codegen, LLVMBuilderRef builder,
  loxc_stmt_print_t print_stmt);
static void if_(codegen_t, LLVMBuilderRef, loxc_stmt_if_t);
static LLVMValueRef binary(codegen_t codegen, LLVMBuilderRef builder,
  loxc_expr_binary_t binary);
static LLVMValueRef logic_binary(codegen_t codegen, LLVMBuilderRef builder,
  loxc_expr_binary_t binary);
static LLVMValueRef negate(codegen_t codegen, LLVMBuilderRef builder,
  loxc_expr_unary_t unary);
static LLVMValueRef truthy(codegen_t codegen, LLVMBuilderRef builder,
  loxc_typeset_t type, LLVMValueRef value);
static LLVMValueRef equal(codegen_t codegen, LLVMBuilderRef builder,
  loxc_expr_t left, loxc_expr_t right);

struct switch_type {
	void (*cont)(codegen_t codegen, LLVMBuilderRef builder, void *user);

	void (*number)(codegen_t codegen, LLVMBuilderRef builder, void *user,
	  LLVMValueRef number);
	void (*boolean)(codegen_t codegen, LLVMBuilderRef builder, void *user,
	  LLVMValueRef boolean);
	void (*nil)(codegen_t codegen, LLVMBuilderRef builder, void *user);

	void (*other)(codegen_t codegen, LLVMBuilderRef builder,
	  void *user);
};

static void switch_type(codegen_t codegen, LLVMBuilderRef builder,
  loxc_typeset_t type, LLVMValueRef value,
  const struct switch_type *callbacks, void *user);
static void set_return(LLVMBuilderRef builder,
  LLVMValueRef *ret, LLVMValueRef this_value);

static void force_type(codegen_t codegen, LLVMBuilderRef builder,
  loxc_analysis_mutation_t mutation);
static LLVMValueRef convert(codegen_t codegen, LLVMBuilderRef builder,
  loxc_typeset_t source_type, loxc_typeset_t target_type, LLVMValueRef value);
static LLVMTypeRef repr(codegen_t codegen, loxc_typeset_t type);
static LLVMTypeRef union_value(codegen_t codegen, loxc_typeset_t type);
static LLVMValueRef bitcast_to_int(codegen_t codegen, LLVMBuilderRef builder,
  LLVMValueRef value, LLVMTypeRef target_type, const char *name);
static LLVMValueRef bitcast_from_int(codegen_t codegen, LLVMBuilderRef builder,
  LLVMValueRef value, LLVMTypeRef target_type, const char *name);

static LLVMValueRef add_function(codegen_t codegen, const char *name,
  LLVMTypeRef ret_type, LLVMTypeRef *param_types, unsigned int n_params);
static LLVMValueRef get_var(codegen_t, LLVMBuilderRef, loxc_span_t position,
  loxc_string_t varname, loxc_analysis_variable_t varinfo, loxc_typeset_t ty);
static void set_var(codegen_t, LLVMBuilderRef, loxc_span_t position,
  loxc_string_t varname, loxc_analysis_variable_t varinfo, loxc_typeset_t ty,
  LLVMValueRef value);
static LLVMValueRef get_local(codegen_t codegen,
  loxc_analysis_variable_t varinfo);
static void set_local(codegen_t codegen,
  loxc_analysis_variable_t varinfo, LLVMValueRef value);
static scope_t get_scope(codegen_t, int scope);

static void undeclared(codegen_t codegen, LLVMBuilderRef builder,
  loxc_span_t position, loxc_string_t name);
static void panic(codegen_t codegen, LLVMBuilderRef builder,
  loxc_span_t position, loxc_string_t msg);
static void build_unreachable(codegen_t codegen, LLVMBuilderRef builder);

static char *to_llvm_name(loxc_string_t name);

#define CHECK_REACH(ret) if (!codegen->reachable) return ret

void loxc_codegen_to_function(LLVMValueRef function,
  loxc_analysis_t analysis, loxc_ast_t ast, loxc_source_t source) {
	LLVMModuleRef module = LLVMGetGlobalParent(function);
	LLVMContextRef context = LLVMGetModuleContext(module);

	struct codegen codegen;
	codegen.module = LLVMGetGlobalParent(function);
	codegen.context = LLVMGetModuleContext(codegen.module);
	codegen.source = source;
	codegen.reachable = true;

	codegen.print_functions[LOXC_PRIMITIVE_NIL] =
	  add_function(&codegen, "lox::print(nil)",
	    LLVMVoidTypeInContext(context), NULL, 0);
	codegen.print_functions[LOXC_PRIMITIVE_BOOL] =
	  add_function(&codegen, "lox::print(bool)",
	    LLVMVoidTypeInContext(context),
	    (LLVMTypeRef []) { LLVMInt1TypeInContext(context) }, 1);
	codegen.print_functions[LOXC_PRIMITIVE_NUMBER] =
	  add_function(&codegen, "lox::print(number)",
	    LLVMVoidTypeInContext(context),
	    (LLVMTypeRef []) { LLVMDoubleTypeInContext(context) }, 1);

	LLVMValueRef panic = add_function(&codegen, "lox::panic",
	  LLVMVoidTypeInContext(context),
	  (LLVMTypeRef []) {
	    LLVMPointerType(LLVMInt8TypeInContext(context), 0),
	    LLVMInt32TypeInContext(context),
	  }, 2);
	LLVMAddAttributeAtIndex(panic, LLVMAttributeFunctionIndex,
	  LLVMCreateEnumAttribute(context,
	    LLVMGetEnumAttributeKindForName("noreturn", 8),
	    1
	  )
	);
	codegen.panic_function = panic;

	size_t n_globals = loxc_analysis_count_variables(ast);
#ifndef NDEBUG
	codegen.n_globals = n_globals;
#endif
	codegen.globals = mem_new_array(struct global, n_globals);

	for (size_t i = 0; i < n_globals; i++) {
		loxc_analysis_varinfo_t info = loxc_analysis_varinfo(ast, i);
		codegen.globals[i].ty = info->type_union;
		LLVMTypeRef ty = repr(&codegen, info->type_union);

		LLVMValueRef global;
		if (ty == NULL) global = NULL;
		else {
			char *llvm_name = to_llvm_name(info->name);
			global = LLVMAddGlobal(codegen.module, ty, llvm_name);
			mem_free(llvm_name);
			LLVMSetInitializer(global, LLVMGetUndef(ty));
			LLVMSetLinkage(global, LLVMInternalLinkage);
		}

		codegen.globals[i].var = global;
	}

	LLVMBuilderRef builder = LLVMCreateBuilderInContext(codegen.context);
	LLVMBasicBlockRef entry =
	  LLVMAppendBasicBlockInContext(codegen.context, function, "entry");
	LLVMPositionBuilderAtEnd(builder, entry);
	codegen_ast(&codegen, builder, ast);
	if (codegen.reachable) LLVMBuildRetVoid(builder);
	LLVMDisposeBuilder(builder);

	mem_free(codegen.globals);
}

static void codegen_scope(codegen_t codegen,
  LLVMBuilderRef builder, loxc_ast_t ast) {
	size_t n_locals = loxc_analysis_count_variables(ast);
	scope_t scope =
	  mem_new_with_array(struct scope, LLVMValueRef, n_locals);
	scope->parent = codegen->scope;
#ifndef NDEBUG
	scope->n_locals = n_locals;
#endif
	codegen->scope = scope;
	codegen_ast(codegen, builder, ast);
	codegen->scope = scope->parent;
	mem_free(scope);
}

static void codegen_ast(codegen_t codegen,
  LLVMBuilderRef builder, loxc_ast_t ast) {
	size_t n_stmts = loxc_ast_count_stmts(ast);
	for (size_t i = 0; i < n_stmts && codegen->reachable; i++)
	  codegen_stmt(codegen, builder, loxc_ast_get_stmt(ast, i));
}

static void codegen_stmt(codegen_t codegen,
  LLVMBuilderRef builder, loxc_stmt_t stmt) {
	switch (stmt->type) {
		case LOXC_STMT_NULL: break;
		case LOXC_STMT_EXPR: {
			loxc_stmt_expr_t expr = (loxc_stmt_expr_t) stmt;
			codegen_expr(codegen, builder, expr->expr);
			CHECK_REACH();
			break;
		}
		case LOXC_STMT_PRINT: {
			loxc_stmt_print_t print_stmt =
			  (loxc_stmt_print_t) stmt;
			CHECK_REACH();
			print(codegen, builder, print_stmt);
			break;
		}
		case LOXC_STMT_DECLARE: {
			loxc_stmt_declare_t declare =
			  (loxc_stmt_declare_t) stmt;
			LLVMValueRef value = codegen_expr(codegen, builder,
			  declare->value);
			CHECK_REACH();
			set_var(codegen, builder,
			  stmt->position, declare->name,
			  loxc_analysis_stmt_variable(stmt),
			  loxc_analysis_expr_type(declare->value),
			  value
			);
			assert(codegen->reachable);
			break;
		}
		case LOXC_STMT_BLOCK: {
			loxc_stmt_block_t block = (loxc_stmt_block_t) stmt;
			codegen_scope(codegen, builder, block->inner);
			CHECK_REACH();
			break;
		}
		case LOXC_STMT_IF: {
			if_(codegen, builder, (loxc_stmt_if_t) stmt);
			CHECK_REACH();
			break;
		}
		default: unreachable();
	}

	size_t n_mutations = loxc_analysis_stmt_count_mutations(stmt);
	for (size_t i = 0; i < n_mutations; i++) {
		force_type(codegen, builder,
		  loxc_analysis_stmt_mutation(stmt, i));
	}
}

LLVMValueRef codegen_expr(codegen_t codegen,
  LLVMBuilderRef builder, loxc_expr_t expr) {
	if (!codegen->reachable) return NULL;

	LLVMValueRef value;

	switch (expr->type) {
		case LOXC_EXPR_NIL:
			value = NULL;
			break;
		case LOXC_EXPR_BOOL: {
			loxc_expr_bool_t boolean = (loxc_expr_bool_t) expr;
			value = LLVMConstInt(
			  LLVMInt1TypeInContext(codegen->context),
			  boolean->value, false);
			break;
		}
		case LOXC_EXPR_NUMBER: {
			loxc_expr_number_t number = (loxc_expr_number_t) expr;
			value = LLVMConstReal(
			  LLVMDoubleTypeInContext(codegen->context),
			  number->value);
			break;
		}
		case LOXC_EXPR_BINARY: {
			loxc_expr_binary_t binary_expr =
			  (loxc_expr_binary_t) expr;
			value = binary(codegen, builder, binary_expr);
			CHECK_REACH(NULL);
			break;
		}
		case LOXC_EXPR_UNARY: {
			loxc_expr_unary_t unary = (loxc_expr_unary_t) expr;
			switch (unary->op) {
				case LOXC_EXPR_NEG:
					value = negate(codegen, builder,
					  unary);
					CHECK_REACH(NULL);
					break;
				case LOXC_EXPR_NOT:
					value = codegen_expr(codegen, builder,
					  unary->arg);
					CHECK_REACH(NULL);
					value = truthy(codegen, builder,
					  loxc_analysis_expr_type(unary->arg),
					  value);
					value = LLVMBuildNot(builder,
					  value, "not");
					assert(codegen->reachable);
					break;
				default: unreachable();
			}
			break;
		}
		case LOXC_EXPR_VARIABLE: {
			loxc_expr_variable_t variable =
			  (loxc_expr_variable_t) expr;
			value = get_var(codegen, builder,
			  expr->position, variable->name,
			  loxc_analysis_expr_variable(expr),
			  loxc_analysis_expr_type(expr));
			CHECK_REACH(NULL);
			break;
		}
		case LOXC_EXPR_ASSIGN: {
			loxc_expr_assign_t assign = (loxc_expr_assign_t) expr;
			value = codegen_expr(codegen, builder, assign->value);
			CHECK_REACH(NULL);
			set_var(codegen, builder,
			  expr->position, assign->name,
			  loxc_analysis_expr_variable(expr),
			  loxc_analysis_expr_type(assign->value),
			  value);
			CHECK_REACH(NULL);
			break;
		}
		default: unreachable();
	}

	size_t n_mutations = loxc_analysis_expr_count_mutations(expr);
	for (size_t i = 0; i < n_mutations; i++) {
		force_type(codegen, builder,
		  loxc_analysis_expr_mutation(expr, i));
	}

	return value;
}

static void print_number(codegen_t codegen, LLVMBuilderRef builder, void *user,
  LLVMValueRef value) {
	LLVMBuildCall(builder, codegen->print_functions[LOXC_PRIMITIVE_NUMBER],
	  (LLVMValueRef []) { value }, 1, "");
}

static void print_bool(codegen_t codegen, LLVMBuilderRef builder, void *user,
  LLVMValueRef value) {
	LLVMBuildCall(builder, codegen->print_functions[LOXC_PRIMITIVE_BOOL],
	  (LLVMValueRef []) { value }, 1, "");
}

static void print_nil(codegen_t codegen, LLVMBuilderRef builder, void *user) {
	LLVMBuildCall(builder, codegen->print_functions[LOXC_PRIMITIVE_NIL],
	  NULL, 0, "");
}

static void print(codegen_t codegen, LLVMBuilderRef builder,
  loxc_stmt_print_t print_stmt) {
	LLVMValueRef value = codegen_expr(codegen, builder, print_stmt->expr);
	CHECK_REACH();

	loxc_typeset_t type = loxc_analysis_expr_type(print_stmt->expr);

	static const struct switch_type print_switch = {
		.number = &print_number,
		.boolean = &print_bool,
		.nil = &print_nil,
	};
	switch_type(codegen, builder, type, value, &print_switch, NULL);
}

static void if_(codegen_t codegen, LLVMBuilderRef builder,
  loxc_stmt_if_t if_stmt) {
	LLVMValueRef condition_value =
	  codegen_expr(codegen, builder, if_stmt->condition);
	loxc_typeset_t condition_type =
	  loxc_analysis_expr_type(if_stmt->condition);
	LLVMValueRef condition_bool =
	  truthy(codegen, builder, condition_type, condition_value);

	LLVMBasicBlockRef previous_block = LLVMGetInsertBlock(builder);
	LLVMValueRef function = LLVMGetBasicBlockParent(previous_block);

	LLVMBasicBlockRef if_block =
	  LLVMAppendBasicBlockInContext(codegen->context, function, "if");

	LLVMBasicBlockRef
	  else_block = LLVMAppendBasicBlockInContext(codegen->context,
	    function, "else"),
	  continue_block = LLVMAppendBasicBlockInContext(codegen->context,
	    function, "continue");

	LLVMBuildCondBr(builder, condition_bool, if_block, else_block);

	/* Building the if is done in five steps:
	   1. Save variables the entire statement may modify.
	   2. Build the if-clause.
	   3. Restore the saved variables, and save the values produced after
	      the if-clause.
	   4. Build the else-clause
	   5. Build phi-nodes to combine the two results.
	 */
	/* Step 1 */
	size_t n_modified =
	  loxc_analysis_stmt_count_mutations(&if_stmt->stmt);
	LLVMValueRef *saved;
	if (n_modified != 0) {
		saved = mem_new_array(LLVMValueRef, n_modified);
		for (size_t i = 0; i < n_modified; i++) {
			loxc_analysis_mutation_t mutation =
			  loxc_analysis_stmt_mutation(&if_stmt->stmt, i);
			/* Globals are not SSA, so no need for tricks */
			if (mutation->variable.scope == -1) continue;
			saved[i] = get_local(codegen, &mutation->variable);
		}
	} else saved = NULL;

	/* Step 2 */
	LLVMPositionBuilderAtEnd(builder, if_block);
	codegen_stmt(codegen, builder, if_stmt->if_);
	bool if_reachable = codegen->reachable;
	if (if_reachable) LLVMBuildBr(builder, continue_block);

	/* Step 3 */
	for (size_t i = 0; i < n_modified; i++) {
		loxc_analysis_mutation_t mutation =
		  loxc_analysis_stmt_mutation(&if_stmt->stmt, i);
		if (mutation->variable.scope == -1) continue;
		LLVMValueRef new_value =
		  get_local(codegen, &mutation->variable);
		set_local(codegen, &mutation->variable, saved[i]);
		saved[i] = new_value;
	}

	/* Step 4 */
	LLVMPositionBuilderAtEnd(builder, else_block);
	codegen->reachable = true;
	codegen_stmt(codegen, builder, if_stmt->else_);
	bool else_reachable = codegen->reachable;
	if (else_reachable) LLVMBuildBr(builder, continue_block);

	/* Step 5 */
	LLVMPositionBuilderAtEnd(builder, continue_block);

	if (!else_reachable) {
		if (if_reachable) {
			codegen->reachable = true;
			for (size_t i = 0; i < n_modified; i++) {
				loxc_analysis_mutation_t mutation =
				  loxc_analysis_stmt_mutation(&if_stmt->stmt,
				    i);
				if (mutation->variable.scope == -1) {
					set_local(codegen,
					  &mutation->variable, saved[i]);
				}
			}
		}
	} else if (if_reachable) {
		LLVMBasicBlockRef incoming_blocks[] = { if_block, else_block };
		for (size_t i = 0; i < n_modified; i++) {
			loxc_analysis_mutation_t mutation =
			  loxc_analysis_stmt_mutation(&if_stmt->stmt, i);
			if (mutation->variable.scope == -1) continue;
			LLVMValueRef phi = LLVMBuildPhi(builder,
			  LLVMTypeOf(saved[i]), "local");
			LLVMAddIncoming(phi,
			  (LLVMValueRef []) {
			    saved[i],
			    get_local(codegen, &mutation->variable)
			  }, incoming_blocks, 2);
			set_local(codegen, &mutation->variable, phi);
		}
	}

	mem_free(saved);
}

typedef struct {
	loxc_span_t position;
	LLVMValueRef value;
} extract_value_t;

static void binary_number(codegen_t codegen, LLVMBuilderRef builder,
  void *user, LLVMValueRef value) {
	extract_value_t *result = user;
	result->value = value;
}

static void binary_other(codegen_t codegen, LLVMBuilderRef builder,
  void *user) {
	extract_value_t *result = user;
	panic(codegen, builder, result->position,
	  loxc_string_from_cstring("Operands must be numbers"));
}

static LLVMValueRef binary(codegen_t codegen, LLVMBuilderRef builder,
  loxc_expr_binary_t binary) {
	switch (binary->op) {
		case LOXC_EXPR_EQ:
			return equal(codegen, builder,
			  binary->left, binary->right);
		case LOXC_EXPR_NE:
			return LLVMBuildNot(builder, equal(codegen, builder,
			  binary->left, binary->right), "not_equal");
		case LOXC_EXPR_AND:
		case LOXC_EXPR_OR:
			return logic_binary(codegen, builder, binary);
		default: break;
	}

	LLVMValueRef left =
	  codegen_expr(codegen, builder, binary->left);
	CHECK_REACH(NULL);
	LLVMValueRef right =
	  codegen_expr(codegen, builder, binary->right);
	CHECK_REACH(NULL);

	static const struct switch_type binary_switch = {
		.number = &binary_number,
		.other = &binary_other,
	};

	extract_value_t left_num = { .position = binary->expr.position };
	switch_type(codegen, builder,
	  loxc_analysis_expr_type(binary->left), left,
	  &binary_switch, &left_num);
	CHECK_REACH(NULL);

	extract_value_t right_num = { .position = binary->expr.position };
	switch_type(codegen, builder,
	  loxc_analysis_expr_type(binary->right), right,
	  &binary_switch, &right_num);
	CHECK_REACH(NULL);

	if (binary->op < LOXC_EXPR_ARITH) {
		static LLVMValueRef (*const build[])(LLVMBuilderRef,
		  LLVMValueRef, LLVMValueRef, const char *name) = {
			[LOXC_EXPR_ADD] = &LLVMBuildFAdd,
			[LOXC_EXPR_SUB] = &LLVMBuildFSub,
			[LOXC_EXPR_MUL] = &LLVMBuildFMul,
			[LOXC_EXPR_DIV] = &LLVMBuildFDiv,
		};
		return (*build[binary->op])(builder,
		  left_num.value, right_num.value, "binary");
	} else {
#define OP(x) [LOXC_EXPR_ ## x - LOXC_EXPR_ARITH] = LLVMRealO ## x
		static const LLVMRealPredicate pred[] =
		  { OP(LT), OP(LE), OP(GT), OP(GE) };
#undef OP
		return LLVMBuildFCmp(builder,
		  pred[binary->op - LOXC_EXPR_ARITH],
		  left_num.value, right_num.value, "compare");
	}
}

static LLVMValueRef logic_binary(codegen_t codegen, LLVMBuilderRef builder,
  loxc_expr_binary_t binary) {
	LLVMValueRef left = codegen_expr(codegen, builder, binary->left);
	CHECK_REACH(NULL);
	loxc_typeset_t left_type = loxc_analysis_expr_type(binary->left);
	LLVMValueRef truth = truthy(codegen, builder, left_type, left);
	assert(codegen->reachable);

	LLVMBasicBlockRef start_block = LLVMGetInsertBlock(builder);
	LLVMValueRef function = LLVMGetBasicBlockParent(start_block);
	LLVMBasicBlockRef rhs_block = LLVMAppendBasicBlock(function, "rhs");
	LLVMPositionBuilderAtEnd(builder, rhs_block);
	LLVMValueRef right = codegen_expr(codegen, builder, binary->right);

	if (codegen->reachable) {
		loxc_typeset_t result_type =
		  loxc_analysis_expr_type(&binary->expr);
		LLVMBasicBlockRef conv_block =
		  LLVMAppendBasicBlock(function, "convert");
		LLVMBasicBlockRef cont =
		  LLVMAppendBasicBlock(function, "continue");
		right = convert(codegen, builder,
		  loxc_analysis_expr_type(binary->right), result_type, right);
		LLVMBuildBr(builder, cont);

		LLVMPositionBuilderAtEnd(builder, start_block);
		if (binary->op == LOXC_EXPR_AND)
		  LLVMBuildCondBr(builder, truth, rhs_block, conv_block);
		else LLVMBuildCondBr(builder, truth, conv_block, rhs_block);

		LLVMPositionBuilderAtEnd(builder, conv_block);
		left = convert(codegen, builder, left_type, result_type, left);
		LLVMBuildBr(builder, cont);

		LLVMPositionBuilderAtEnd(builder, cont);
		LLVMTypeRef ty = repr(codegen, result_type);
		if (ty == NULL) return NULL;
		LLVMValueRef phi = LLVMBuildPhi(builder, ty, "result");
		LLVMAddIncoming(phi,
		  (LLVMValueRef []) { left, right },
		  (LLVMBasicBlockRef []) { conv_block, rhs_block },
		  2);
		return phi;
	} else {
		codegen->reachable = true;
		LLVMBasicBlockRef cont =
		  LLVMAppendBasicBlock(function, "continue");

		LLVMPositionBuilderAtEnd(builder, start_block);
		if (binary->op == LOXC_EXPR_AND)
		  LLVMBuildCondBr(builder, truth, rhs_block, cont);
		else LLVMBuildCondBr(builder, truth, cont, rhs_block);

		LLVMPositionBuilderAtEnd(builder, cont);
		return left;
	}
}

static void negate_number(codegen_t codegen, LLVMBuilderRef builder,
  void *user, LLVMValueRef value) {
	extract_value_t *result = user;
	result->value = value;
}

static void negate_other(codegen_t codegen, LLVMBuilderRef builder,
  void *user) {
	extract_value_t *result = user;
	panic(codegen, builder, result->position,
	  loxc_string_from_cstring("Operand must be a number"));
}

static LLVMValueRef negate(codegen_t codegen, LLVMBuilderRef builder,
  loxc_expr_unary_t unary) {
	LLVMValueRef arg = codegen_expr(codegen, builder, unary->arg);
	CHECK_REACH(NULL);

	static const struct switch_type negate_switch = {
		.number = &negate_number,
		.other = &negate_other,
	};
	extract_value_t num = { .position = unary->expr.position };
	switch_type(codegen, builder,
	  loxc_analysis_expr_type(unary->arg), arg, &negate_switch, &num);
	CHECK_REACH(NULL);

	return LLVMBuildFNeg(builder, num.value, "negate");
} 

static void truthy_cont(codegen_t codegen, LLVMBuilderRef builder,
  void *user) {
	LLVMValueRef *truthy = user;
	*truthy = LLVMBuildPhi(builder,
	  LLVMInt1TypeInContext(codegen->context), "truthy");
}

static void truthy_nil(codegen_t codegen, LLVMBuilderRef builder, void *user) {
	LLVMValueRef *truthy = user;
	LLVMValueRef falsy =
	  LLVMConstInt(LLVMInt1TypeInContext(codegen->context), false, false);
	set_return(builder, truthy, falsy);
}

static void truthy_bool(codegen_t codegen, LLVMBuilderRef builder, void *user,
  LLVMValueRef boolean) {
	LLVMValueRef *truthy = user;
	set_return(builder, truthy, boolean);
}

static void truthy_other(codegen_t codegen, LLVMBuilderRef builder,
  void *user) {
	LLVMValueRef *truthy = user;
	LLVMValueRef truth =
	  LLVMConstInt(LLVMInt1TypeInContext(codegen->context), true, false);
	set_return(builder, truthy, truth);
}

static LLVMValueRef truthy(codegen_t codegen, LLVMBuilderRef builder,
  loxc_typeset_t type, LLVMValueRef value) {
	LLVMValueRef truthy = NULL;

	static const struct switch_type truthy_switch = {
		.cont = &truthy_cont,
		.nil = &truthy_nil,
		.boolean = &truthy_bool,
		.other = &truthy_other,
	};

	switch_type(codegen, builder, type, value, &truthy_switch, &truthy);
	return truthy;
}

typedef struct {
	LLVMValueRef lhs;
	LLVMValueRef result;
} eq_rhs_t;

static void eq_nil(codegen_t codegen, LLVMBuilderRef builder,
  void *user) {
	eq_rhs_t *eq = user;
	set_return(builder, &eq->result,
	  LLVMConstInt(LLVMInt1TypeInContext(codegen->context), true, false));
}

static void eq_bool(codegen_t codegen, LLVMBuilderRef builder,
  void *user, LLVMValueRef rhs) {
	eq_rhs_t *eq = user;
	set_return(builder, &eq->result,
	  LLVMBuildICmp(builder, LLVMIntEQ, eq->lhs, rhs, "equal_bool"));
}

static void eq_number(codegen_t codegen, LLVMBuilderRef builder,
  void *user, LLVMValueRef rhs) {
	eq_rhs_t *eq = user;
	set_return(builder, &eq->result,
	  LLVMBuildFCmp(builder, LLVMRealOEQ, eq->lhs, rhs,
	    "equal_number"));
}

static void eq_other(codegen_t codegen, LLVMBuilderRef builder,
  void *user) {
	eq_rhs_t *eq = user;
	set_return(builder, &eq->result,
	  LLVMConstInt(LLVMInt1TypeInContext(codegen->context), false, false));
}

static void eq_cont(codegen_t codegen, LLVMBuilderRef builder,
  void *user) {
	eq_rhs_t *eq = user;
	eq->result = LLVMBuildPhi(builder,
	  LLVMInt1TypeInContext(codegen->context), "equal");
}

typedef struct {
	loxc_typeset_t rhs_type;
	LLVMValueRef rhs;
	LLVMValueRef result;
} eq_lhs_t;

static void eq_lhs_nil(codegen_t codegen, LLVMBuilderRef builder,
  void *user) {
	eq_lhs_t *lhs = user;
	eq_rhs_t rhs = { .result = NULL };
	static const struct switch_type switch_nil = {
		.nil = &eq_nil,
		.other = &eq_other,
		.cont = &eq_cont,
	};
	switch_type(codegen, builder, lhs->rhs_type, lhs->rhs,
	  &switch_nil, &rhs);
	set_return(builder, &lhs->result, rhs.result);
}

static void eq_lhs_bool(codegen_t codegen, LLVMBuilderRef builder,
  void *user, LLVMValueRef boolean) {
	eq_lhs_t *lhs = user;
	eq_rhs_t rhs = { .lhs = boolean, .result = NULL };
	static const struct switch_type switch_bool = {
		.boolean = &eq_bool,
		.other = &eq_other,
		.cont = &eq_cont,
	};
	switch_type(codegen, builder, lhs->rhs_type, lhs->rhs,
	  &switch_bool, &rhs);
	set_return(builder, &lhs->result, rhs.result);
}

static void eq_lhs_number(codegen_t codegen, LLVMBuilderRef builder,
  void *user, LLVMValueRef number) {
	eq_lhs_t *lhs = user;
	eq_rhs_t rhs = { .lhs = number, .result = NULL };
	static const struct switch_type switch_number = {
		.number = &eq_number,
		.other = &eq_other,
		.cont = &eq_cont,
	};
	switch_type(codegen, builder, lhs->rhs_type, lhs->rhs,
	  &switch_number, &rhs);
	set_return(builder, &lhs->result, rhs.result);
}

static void eq_lhs_cont(codegen_t codegen, LLVMBuilderRef builder,
  void *user) {
	eq_lhs_t *eq = user;
	eq->result = LLVMBuildPhi(builder,
	  LLVMInt1TypeInContext(codegen->context), "equal");
}

static LLVMValueRef equal(codegen_t codegen, LLVMBuilderRef builder,
  loxc_expr_t left, loxc_expr_t right) {
	LLVMValueRef lhs = codegen_expr(codegen, builder, left);
	CHECK_REACH(NULL);
	LLVMValueRef rhs = codegen_expr(codegen, builder, right);
	CHECK_REACH(NULL);

	static const struct switch_type switch_equal = {
		.nil = &eq_lhs_nil,
		.boolean = &eq_lhs_bool,
		.number = &eq_lhs_number,
		.cont = &eq_lhs_cont,
	};
	eq_lhs_t eq = {
		.rhs_type = loxc_analysis_expr_type(right),
		.rhs = rhs,
		.result = NULL,
	};
	switch_type(codegen, builder, loxc_analysis_expr_type(left), lhs,
	  &switch_equal, &eq);
	return eq.result;
}

static void switch_type(codegen_t codegen, LLVMBuilderRef builder,
  loxc_typeset_t type, LLVMValueRef value,
  const struct switch_type *callbacks, void *user) {

	loxc_primitive_set_t prim = loxc_typeset_primitive_set(type);
	if (prim == LOXC_PRIMITIVE_SET_NUMBER) {
		if (callbacks->number != NULL)
		  (*callbacks->number)(codegen, builder, user, value);
		else
		  (*callbacks->other)(codegen, builder, user);
	} else if (prim == LOXC_PRIMITIVE_SET_BOOL) {
		if (callbacks->boolean != NULL)
		  (*callbacks->boolean)(codegen, builder, user, value);
		else
		  (*callbacks->other)(codegen, builder, user);
	} else if (prim == LOXC_PRIMITIVE_SET_NIL) {
		if (callbacks->nil != NULL)
		  (*callbacks->nil)(codegen, builder, user);
		else
		  (*callbacks->other)(codegen, builder, user);
	} else if (prim ==
	  (LOXC_PRIMITIVE_SET_NIL | LOXC_PRIMITIVE_SET_BOOL)) {
		if (callbacks->boolean == NULL && callbacks->nil == NULL) {
			(*callbacks->other)(codegen, builder, user);
			return;
		}

		LLVMBasicBlockRef previous_block = LLVMGetInsertBlock(builder);
		LLVMValueRef function =
		  LLVMGetBasicBlockParent(previous_block);

		// TODO: reachability
		LLVMValueRef is_nil = LLVMBuildICmp(builder, LLVMIntEQ,
		  value,
		  LLVMConstInt(LLVMIntTypeInContext(codegen->context, 2),
		    0, false),
		  "is_nil");

		LLVMBasicBlockRef case_nil =
		  LLVMAppendBasicBlockInContext(codegen->context,
		    function, "nil");
		LLVMBasicBlockRef case_bool =
		  LLVMAppendBasicBlockInContext(codegen->context,
		    function, "bool");

		LLVMBuildCondBr(builder, is_nil, case_nil, case_bool);

		LLVMBasicBlockRef cont =
		  LLVMAppendBasicBlockInContext(codegen->context,
		    function, "continue");
		if (callbacks->cont != NULL) {
			LLVMPositionBuilderAtEnd(builder, cont);
			(*callbacks->cont)(codegen, builder, user);
		}
		bool cont_reachable = codegen->reachable;

		bool any_reachable = false;
		codegen->reachable = true;

		LLVMPositionBuilderAtEnd(builder, case_nil);
		if (callbacks->nil != NULL)
		  (*callbacks->nil)(codegen, builder, user);
		else
		  (*callbacks->other)(codegen, builder, user);
		if (codegen->reachable) LLVMBuildBr(builder, cont);
		any_reachable |= codegen->reachable;
		codegen->reachable = true;

		LLVMPositionBuilderAtEnd(builder, case_bool);
		if (callbacks->boolean != NULL) {
			LLVMValueRef boolean = LLVMBuildTrunc(builder, value,
			  LLVMInt1TypeInContext(codegen->context), "bool");
			(*callbacks->boolean)(codegen, builder, user, boolean);
		} else (*callbacks->other)(codegen, builder, user);
		if (codegen->reachable) LLVMBuildBr(builder, cont);
		any_reachable |= codegen->reachable;

		codegen->reachable = any_reachable & cont_reachable;

		LLVMPositionBuilderAtEnd(builder, cont);
	} else {
		unsigned num_cases = 0;
		LLVMBasicBlockRef case_number, case_bool, case_nil;

		LLVMBasicBlockRef previous_block = LLVMGetInsertBlock(builder);
		LLVMValueRef function =
		  LLVMGetBasicBlockParent(previous_block);

		if (prim & LOXC_PRIMITIVE_SET_NUMBER &&
		  callbacks->number != NULL) {
			case_number =
			  LLVMAppendBasicBlockInContext(codegen->context,
			    function, "number");
			num_cases++;
		} else case_number = NULL;

		if (prim & LOXC_PRIMITIVE_SET_BOOL &&
		  callbacks->boolean != NULL) {
			case_bool =
			  LLVMAppendBasicBlockInContext(codegen->context,
			    function, "bool");
			num_cases++;
		} else case_bool = NULL;

		if (prim & LOXC_PRIMITIVE_SET_NIL &&
		  callbacks->nil != NULL) {
			case_nil =
			  LLVMAppendBasicBlockInContext(codegen->context,
			    function, "nil");
			num_cases++;
		} else case_nil = NULL;

		LLVMValueRef type_discr = LLVMBuildExtractValue(builder,
		  value, 0, "type");

		LLVMBasicBlockRef other =
		  LLVMAppendBasicBlockInContext(codegen->context,
		    function, "other");
		LLVMValueRef the_switch =
		  LLVMBuildSwitch(builder, type_discr, other, num_cases);

		LLVMBasicBlockRef cont =
		  LLVMAppendBasicBlockInContext(codegen->context,
		    function, "continue");
		if (callbacks->cont != NULL) {
			LLVMPositionBuilderAtEnd(builder, cont);
			(*callbacks->cont)(codegen, builder, user);
		}
		bool cont_reachable = codegen->reachable;
		codegen->reachable = true;

		bool any_reachable = false;

		if (case_number != NULL) {
			LLVMAddCase(the_switch,
			  LLVMConstInt(LLVMInt8TypeInContext(codegen->context),
			    LOXC_PRIMITIVE_NUMBER, false),
			  case_number);
			LLVMPositionBuilderAtEnd(builder, case_number);
			LLVMValueRef just_value =
			  LLVMBuildExtractValue(builder, value, 1, "value");
			LLVMValueRef num_value =
			  bitcast_from_int(codegen, builder, just_value,
			    LLVMDoubleTypeInContext(codegen->context),
			    "number");
			(*callbacks->number)(codegen, builder, user,
			  num_value);
			if (codegen->reachable) LLVMBuildBr(builder, cont);
			any_reachable |= codegen->reachable;
			codegen->reachable = true;
		}

		if (case_bool != NULL) {
			LLVMAddCase(the_switch,
			  LLVMConstInt(LLVMInt8TypeInContext(codegen->context),
			    LOXC_PRIMITIVE_BOOL, false),
			  case_bool);
			LLVMPositionBuilderAtEnd(builder, case_bool);
			LLVMValueRef just_value =
			  LLVMBuildExtractValue(builder, value, 1, "value");
			LLVMValueRef boolean =
			  LLVMBuildTrunc(builder, just_value,
			    LLVMInt1TypeInContext(codegen->context), "bool");
			(*callbacks->boolean)(codegen, builder, user, boolean);
			if (codegen->reachable) LLVMBuildBr(builder, cont);
			any_reachable |= codegen->reachable;
			codegen->reachable = true;
		}

		if (case_nil != NULL) {
			LLVMAddCase(the_switch,
			  LLVMConstInt(LLVMInt8TypeInContext(codegen->context),
			    LOXC_PRIMITIVE_NIL, false),
			  case_nil);
			LLVMPositionBuilderAtEnd(builder, case_nil);
			(*callbacks->nil)(codegen, builder, user);
			if (codegen->reachable) LLVMBuildBr(builder, cont);
			any_reachable |= codegen->reachable;
			codegen->reachable = true;
		}

		LLVMPositionBuilderAtEnd(builder, other);
		if (callbacks->other != NULL)
		  (*callbacks->other)(codegen, builder, user);
		else
		  build_unreachable(codegen, builder);
		if (codegen->reachable) LLVMBuildBr(builder, cont);
		any_reachable |= codegen->reachable;

		codegen->reachable = any_reachable & cont_reachable;

		LLVMPositionBuilderAtEnd(builder, cont);
	}
}

static void set_return(LLVMBuilderRef builder,
  LLVMValueRef *ret, LLVMValueRef this_value) {
	LLVMValueRef phi = *ret;
	if (phi != NULL) {
		LLVMAddIncoming(phi,
		  (LLVMValueRef []) { this_value },
		  (LLVMBasicBlockRef []) { LLVMGetInsertBlock(builder) },
		  1);
	} else *ret = this_value;
}

static void force_type(codegen_t codegen, LLVMBuilderRef builder,
  loxc_analysis_mutation_t mutation) {
	int scope_idx = mutation->variable.scope;
	if (scope_idx < 0) {
		// global: we don't need to do anything, because globals are
		// always converted to their union type.
		return;
	}

	if (mutation->force_type == NULL) return;

	scope_t scope = codegen->scope;
	for (; scope_idx > 0; scope_idx--) {
		assert(scope != NULL);
		scope = scope->parent;
	}
	assert(scope != NULL);

	int index = mutation->variable.index;
	assert(0 <= index && index < (int) scope->n_locals);

	scope->locals[index] = convert(codegen, builder,
	  mutation->type, mutation->force_type, scope->locals[index]);
}

static LLVMValueRef convert(codegen_t codegen, LLVMBuilderRef builder,
  loxc_typeset_t source_type, loxc_typeset_t target_type, LLVMValueRef value) {
	loxc_primitive_set_t
	  source_prim = loxc_typeset_primitive_set(source_type),
	  target_prim = loxc_typeset_primitive_set(target_type);

	if (source_prim == target_prim) return value;

	if (target_prim == LOXC_PRIMITIVE_SET_EMPTY
	  || source_prim == LOXC_PRIMITIVE_SET_EMPTY
	  || target_prim == LOXC_PRIMITIVE_SET_NIL) return NULL;

	/* If the typesets don't share any types, just generate an undef, this
	   code path is unreachable anyway.                       */
	if (!(target_prim & source_prim))
	  return LLVMGetUndef(repr(codegen, target_type));

	if (source_prim == LOXC_PRIMITIVE_SET_NIL) {
		if (target_prim ==
		  (LOXC_PRIMITIVE_SET_NIL | LOXC_PRIMITIVE_SET_BOOL)) {
			return LLVMConstInt(
			  LLVMIntTypeInContext(codegen->context, 2),
			  0, false);
		}

		return LLVMConstStructInContext(codegen->context,
		  (LLVMValueRef []) {
		    LLVMConstInt(LLVMInt8TypeInContext(codegen->context),
		      LOXC_PRIMITIVE_NIL, false),
		    LLVMConstInt(union_value(codegen, target_type),
		      0, false),
		  }, 2, false);
	} else if (source_prim == LOXC_PRIMITIVE_SET_BOOL) {
		if (target_prim ==
		  (LOXC_PRIMITIVE_SET_BOOL | LOXC_PRIMITIVE_SET_NIL)) {
			LLVMTypeRef i2 =
			  LLVMIntTypeInContext(codegen->context, 2);
			LLVMValueRef zext =
			  LLVMBuildZExt(builder, value, i2, "zext");
			LLVMValueRef result = LLVMBuildOr(builder, zext,
			  LLVMConstInt(i2, 2, false), "bool_or_nil");
			return result;
		}

		LLVMTypeRef value_type = union_value(codegen, target_type);
		LLVMValueRef tmp = LLVMConstStructInContext(codegen->context,
		  (LLVMValueRef []) {
		    LLVMConstInt(LLVMInt8TypeInContext(codegen->context),
		      LOXC_PRIMITIVE_BOOL, false),
		    LLVMConstInt(value_type, 0, false),
		  }, 2, false);
		LLVMValueRef zext = LLVMBuildZExt(builder, value,
		  union_value(codegen, target_type), "zext");
		return
		  LLVMBuildInsertValue(builder, tmp, zext, 1, "tagged_union");
	} else if (source_prim == LOXC_PRIMITIVE_SET_NUMBER) {
		LLVMTypeRef value_type = union_value(codegen, target_type);
		LLVMValueRef tmp = LLVMConstStructInContext(codegen->context,
		  (LLVMValueRef []) {
		    LLVMConstInt(LLVMInt8TypeInContext(codegen->context),
		      LOXC_PRIMITIVE_NUMBER, false),
		    LLVMConstInt(value_type, 0, false),
		  }, 2, false);
		value =
		  bitcast_to_int(codegen, builder, value, value_type, "value");
		return
		  LLVMBuildInsertValue(builder, tmp, value, 1, "tagged_union");
	} else if (source_prim ==
	  (LOXC_PRIMITIVE_SET_BOOL | LOXC_PRIMITIVE_SET_NIL)) {
		if (target_prim == LOXC_PRIMITIVE_SET_BOOL) {
			return LLVMBuildTrunc(builder, value,
			  LLVMInt1TypeInContext(codegen->context), "bool");
		}

		LLVMTypeRef i2 = LLVMIntTypeInContext(codegen->context, 2);
		LLVMTypeRef value_type = union_value(codegen, target_type);
		LLVMValueRef function =
		  LLVMGetBasicBlockParent(LLVMGetInsertBlock(builder));

		LLVMValueRef is_nil = LLVMBuildICmp(builder, LLVMIntEQ,
		  value,
		  LLVMConstInt(LLVMIntTypeInContext(codegen->context, 2),
		    0, false),
		  "is_nil");

		LLVMBasicBlockRef case_nil =
		  LLVMAppendBasicBlockInContext(codegen->context,
		    function, "nil");
		LLVMBasicBlockRef case_bool =
		  LLVMAppendBasicBlockInContext(codegen->context,
		    function, "bool");

		LLVMBuildCondBr(builder, is_nil, case_nil, case_bool);

		LLVMBasicBlockRef cont =
		  LLVMAppendBasicBlockInContext(codegen->context,
		    function, "continue");

		LLVMPositionBuilderAtEnd(builder, case_nil);
		LLVMValueRef nil_result;
		if (target_prim & LOXC_PRIMITIVE_SET_NIL)
		  nil_result = LLVMConstStructInContext(codegen->context,
		    (LLVMValueRef []) {
		      LLVMConstInt(LLVMInt8TypeInContext(codegen->context),
		        LOXC_PRIMITIVE_NIL, false),
		      LLVMConstInt(value_type, 0, false),
		    }, 2, false);
		else nil_result = LLVMGetUndef(repr(codegen, target_type));
		LLVMBuildBr(builder, cont);

		LLVMPositionBuilderAtEnd(builder, case_bool);
		LLVMValueRef bool_result;
		if (target_prim & LOXC_PRIMITIVE_SET_BOOL) {
			LLVMValueRef tmp =
			  LLVMConstStructInContext(codegen->context,
			    (LLVMValueRef []) {
			      LLVMConstInt(
			        LLVMInt8TypeInContext(codegen->context),
			          LOXC_PRIMITIVE_BOOL, false),
			      LLVMConstInt(value_type, 0, false),
			    }, 2, false);
			LLVMValueRef bool_raw =
			  LLVMBuildAnd(builder, value,
			    LLVMConstInt(i2, 1, false), "bool");
			LLVMValueRef zext =
			  LLVMBuildZExt(builder, bool_raw, value_type, "zext");
			bool_result = LLVMBuildInsertValue(builder,
			  tmp, zext, 1, "tagged_union_bool");
		} else bool_result = LLVMGetUndef(repr(codegen, target_type));
		LLVMBuildBr(builder, cont);

		LLVMPositionBuilderAtEnd(builder, cont);
		LLVMValueRef result = LLVMBuildPhi(builder,
		  LLVMStructTypeInContext(codegen->context, (LLVMTypeRef []) {
		    LLVMInt8TypeInContext(codegen->context),
		    value_type,
		  }, 2, false), "tagged_union");
		LLVMAddIncoming(result,
		  (LLVMValueRef      []) { nil_result, bool_result },
		  (LLVMBasicBlockRef []) { case_nil,   case_bool }, 2);
		return result;
	}

	/* The source is a tagged union */
	if (target_prim == LOXC_PRIMITIVE_SET_BOOL) {
		value = LLVMBuildExtractValue(builder, value, 1, "value");
		return LLVMBuildTrunc(builder, value,
		  LLVMInt1TypeInContext(codegen->context), "bool");
	} else if (target_prim == LOXC_PRIMITIVE_SET_NUMBER) {
		value = LLVMBuildExtractValue(builder, value, 1, "value");
		return bitcast_from_int(codegen, builder, value,
		  LLVMDoubleTypeInContext(codegen->context), "number");
	} else if (target_prim ==
	  (LOXC_PRIMITIVE_SET_BOOL | LOXC_PRIMITIVE_SET_NIL)) {
		LLVMTypeRef i2 = LLVMIntTypeInContext(codegen->context, 2);
		LLVMValueRef type_discr =
		  LLVMBuildExtractValue(builder, value, 0, "type");
		LLVMValueRef is_bool =
		  LLVMBuildICmp(builder, LLVMIntEQ, type_discr,
		    LLVMConstInt(LLVMInt8TypeInContext(codegen->context),
		      LOXC_PRIMITIVE_BOOL, false), "is_bool");
		is_bool = LLVMBuildZExt(builder, is_bool, i2, "is_bool_i2");
		value = LLVMBuildExtractValue(builder, value, 1, "value");
		value = LLVMBuildTrunc(builder, value, i2, "trunc");
		value = LLVMBuildAnd(builder, is_bool, value, "value");
		LLVMValueRef type_bit = LLVMBuildShl(builder, is_bool,
		  LLVMConstInt(i2, 1, false), "type_bit");
		return LLVMBuildOr(builder, type_bit, value, "result");
	}

	LLVMTargetDataRef data_layout =
	  LLVMGetModuleDataLayout(codegen->module);

	/* We have established that both source and destination are represented
	   as tagged unions.            */
	LLVMTypeRef target_value = union_value(codegen, target_type);
	unsigned target_bits = LLVMSizeOfTypeInBits(data_layout, target_value);

	LLVMTypeRef source_value = union_value(codegen, source_type);
	unsigned source_bits = LLVMSizeOfTypeInBits(data_layout, source_value);

	if (target_bits == source_bits) return value;

	LLVMValueRef new_value = LLVMConstStructInContext(codegen->context,
	  (LLVMValueRef []) {
	    LLVMConstInt(LLVMInt8TypeInContext(codegen->context), 0, false),
	    LLVMConstInt(target_value, 0, false),
	  }, 2, false);

	LLVMValueRef type_discr =
	  LLVMBuildExtractValue(builder, value, 0, "type");
	new_value =
	  LLVMBuildInsertValue(builder, new_value, type_discr, 0, "");

	LLVMValueRef actual_value =
	  LLVMBuildExtractValue(builder, value, 1, "value");
	if (target_bits > source_bits) {
		actual_value = LLVMBuildZExt(builder,
		  actual_value, target_value, "resize_int");
	} else {
		actual_value = LLVMBuildTrunc(builder,
		  actual_value, target_value, "resize_int");
	}
	new_value =
	  LLVMBuildInsertValue(builder, new_value, actual_value, 1, "convert");

	return new_value;
}

static LLVMTypeRef repr(codegen_t codegen, loxc_typeset_t type) {
	loxc_primitive_set_t prim = loxc_typeset_primitive_set(type);

	if (prim == LOXC_PRIMITIVE_SET_EMPTY
	  || prim == LOXC_PRIMITIVE_SET_NIL) return NULL;

	if (prim == LOXC_PRIMITIVE_SET_NUMBER)
	  return LLVMDoubleTypeInContext(codegen->context);

	if (prim == LOXC_PRIMITIVE_SET_BOOL)
	  return LLVMInt1TypeInContext(codegen->context);

	if (prim == (LOXC_PRIMITIVE_SET_BOOL | LOXC_PRIMITIVE_SET_NIL))
	  return LLVMIntTypeInContext(codegen->context, 2);

	return LLVMStructTypeInContext(codegen->context, (LLVMTypeRef []) {
	  LLVMInt8TypeInContext(codegen->context),
	  union_value(codegen, type),
	}, 2, false);
}

static LLVMTypeRef union_value(codegen_t codegen, loxc_typeset_t type) {
	loxc_primitive_set_t prim = loxc_typeset_primitive_set(type);

	unsigned len = 0;

	if (prim & LOXC_PRIMITIVE_SET_NUMBER && len < 64) len = 64;
	if (prim & LOXC_PRIMITIVE_SET_BOOL && len < 1) len = 1;

	return LLVMIntTypeInContext(codegen->context, len);
}

static LLVMValueRef bitcast_to_int(codegen_t codegen, LLVMBuilderRef builder,
  LLVMValueRef value, LLVMTypeRef target_type, const char *name) {
	LLVMTypeRef source_type = LLVMTypeOf(value);
	LLVMTargetDataRef data_layout =
	  LLVMGetModuleDataLayout(codegen->module);

	unsigned long long
	  source_bits = LLVMSizeOfTypeInBits(data_layout, source_type),
	  target_bits = LLVMSizeOfTypeInBits(data_layout, target_type);

	LLVMTypeRef source_as_int =
	  LLVMIntTypeInContext(codegen->context, source_bits);

	value = LLVMBuildBitCast(builder, value, source_as_int, name);

	if (source_bits != target_bits) {
		assert(source_bits < target_bits);
		value = LLVMBuildZExt(builder, value, target_type, name);
	}

	return value;
}

static LLVMValueRef bitcast_from_int(codegen_t codegen, LLVMBuilderRef builder,
  LLVMValueRef value, LLVMTypeRef target_type, const char *name) {
	LLVMTypeRef source_type = LLVMTypeOf(value);
	LLVMTargetDataRef data_layout =
	  LLVMGetModuleDataLayout(codegen->module);

	unsigned long long
	  source_bits = LLVMSizeOfTypeInBits(data_layout, source_type),
	  target_bits = LLVMSizeOfTypeInBits(data_layout, target_type);

	if (source_bits != target_bits) {
		assert(source_bits > target_bits);
		value = LLVMBuildTrunc(builder, value,
		  LLVMIntTypeInContext(codegen->context, target_bits),
		  "trunc");
	}

	return LLVMBuildBitCast(builder, value, target_type, name);
}

static LLVMValueRef add_function(codegen_t codegen, const char *name,
  LLVMTypeRef ret_type, LLVMTypeRef *param_types, unsigned int n_params) {
	LLVMValueRef function = LLVMAddFunction(codegen->module,
	  name,
	  LLVMFunctionType(ret_type, param_types, n_params, false));
	LLVMSetFunctionCallConv(function, LLVMFastCallConv);
	return function;
}

static LLVMValueRef get_var(codegen_t codegen,
  LLVMBuilderRef builder, loxc_span_t position, loxc_string_t varname,
  loxc_analysis_variable_t varinfo, loxc_typeset_t ty) {
	if (varinfo->scope == -1) {
		/* Global variable */
		int index = varinfo->index;
		if (index == -1) {
			undeclared(codegen, builder, position, varname);
			return NULL;
		}

		assert(index < (int) codegen->n_globals);
		LLVMValueRef value =
		  LLVMBuildLoad(builder, codegen->globals[index].var,
		  "global");
		return convert(codegen, builder,
		  codegen->globals[index].ty, ty, value);
	} else return get_local(codegen, varinfo);
}

static void set_var(codegen_t codegen,
  LLVMBuilderRef builder, loxc_span_t position, loxc_string_t varname,
  loxc_analysis_variable_t varinfo, loxc_typeset_t ty, LLVMValueRef value) {
	if (varinfo->scope == -1) {
		/* Global variable */
		int index = varinfo->index;
		if (index == -1) {
			undeclared(codegen, builder, position, varname);
			return;
		}

		assert((size_t) index < codegen->n_globals);
		value = convert(codegen, builder, ty,
		  codegen->globals[index].ty, value);
		if (value != NULL)
		  LLVMBuildStore(builder, value, codegen->globals[index].var);
	} else set_local(codegen, varinfo, value);
}

static LLVMValueRef get_local(codegen_t codegen,
  loxc_analysis_variable_t varinfo) {
	scope_t scope = get_scope(codegen, varinfo->scope);
	int index = varinfo->index;
	assert(0 <= index && index < (int) scope->n_locals);
	LLVMValueRef value = scope->locals[index];
	return value;
}

static void set_local(codegen_t codegen,
  loxc_analysis_variable_t varinfo, LLVMValueRef value) {
	scope_t scope = get_scope(codegen, varinfo->scope);
	int index = varinfo->index;
	assert(0 <= index && index < (int) scope->n_locals);
	scope->locals[index] = value;
}

static scope_t get_scope(codegen_t codegen, int scope_info) {
	scope_t scope = codegen->scope;
	assert(scope != NULL);
	while (scope_info > 0) {
		scope_info--;
		scope = scope->parent;
		assert(scope != NULL);
	}
	return scope;
}

static void undeclared(codegen_t codegen, LLVMBuilderRef instr_builder,
  loxc_span_t position, loxc_string_t name) {
	loxc_string_builder_t builder;
	loxc_string_builder_init(&builder);
	loxc_string_builder_append(&builder,
	  loxc_string_from_cstring("Variable `"));
	loxc_string_builder_append(&builder, name);
	loxc_string_builder_append(&builder,
	  loxc_string_from_cstring("' not declared"));
	loxc_string_t msg = loxc_string_builder_complete(&builder);
	panic(codegen, instr_builder, position, msg);
	loxc_string_free(msg);
}

static void panic(codegen_t codegen, LLVMBuilderRef instr_builder,
  loxc_span_t position, loxc_string_t msg) {
	loxc_string_builder_t builder;
	loxc_string_builder_init(&builder);
	loxc_string_builder_appendf(&builder, "%s:%u: ",
	  loxc_source_filename(codegen->source), position.start.line);
	loxc_string_builder_append(&builder, msg);
	loxc_string_t full_msg = loxc_string_builder_complete(&builder);

	LLVMValueRef constant = LLVMAddGlobal(codegen->module,
	  LLVMArrayType(LLVMInt8TypeInContext(codegen->context),
	    full_msg.length), "error_message");
	LLVMSetLinkage(constant, LLVMPrivateLinkage);
	LLVMSetInitializer(constant, LLVMConstStringInContext(codegen->context,
	  full_msg.chars, full_msg.length, true));
	LLVMSetGlobalConstant(constant, true);

	loxc_string_free(full_msg);

	LLVMTypeRef i32 = LLVMInt32TypeInContext(codegen->context);
	LLVMValueRef zero = LLVMConstInt(i32, 0, false);
	LLVMValueRef pointer = LLVMBuildGEP(instr_builder, constant,
	  (LLVMValueRef []) { zero, zero }, 2, "error_message");

	LLVMBuildCall(instr_builder, codegen->panic_function,
	  (LLVMValueRef []) {
	    pointer,
	    LLVMConstInt(i32, full_msg.length, false),
	  }, 2, "");
	build_unreachable(codegen, instr_builder);
}

static void build_unreachable(codegen_t codegen, LLVMBuilderRef builder) {
	codegen->reachable = false;
	LLVMBuildUnreachable(builder);
}

static char *to_llvm_name(loxc_string_t name) {
	char *llvm_name = malloc(name.length + 5);
	memcpy(llvm_name, "lox:", 4);
	memcpy(llvm_name + 4, name.chars, name.length);
	llvm_name[name.length + 4] = '\0';
	return llvm_name;
}
