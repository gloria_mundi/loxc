
#ifndef LOXC_ANALYSIS_H
#define LOXC_ANALYSIS_H

#include <stddef.h>

#include "type.h"
#include "logger.h"
#include "ast.h"

typedef struct loxc_analysis *loxc_analysis_t;

typedef struct loxc_analysis_varinfo {
	loxc_string_t name;
	loxc_typeset_t type_union;
} *loxc_analysis_varinfo_t;

typedef struct loxc_analysis_variable {
	int scope;
	int index;
} *loxc_analysis_variable_t;

typedef struct loxc_analysis_mutation {
	struct loxc_analysis_variable variable;
	loxc_typeset_t type, force_type;
} *loxc_analysis_mutation_t;

/* Analyse the given AST. This needs to be done before any other functions
   from this module are used on parts of the AST.                          */
loxc_analysis_t loxc_analyse(loxc_ast_t ast, loxc_source_t source,
  loxc_logger_t logger);
void loxc_analysis_free(loxc_analysis_t);

size_t loxc_analysis_count_variables(loxc_ast_t ast);
loxc_analysis_varinfo_t loxc_analysis_varinfo(loxc_ast_t ast, size_t index);

loxc_analysis_variable_t loxc_analysis_stmt_variable(loxc_stmt_t stmt);
loxc_analysis_variable_t loxc_analysis_expr_variable(loxc_expr_t expr);

size_t loxc_analysis_stmt_count_mutations(loxc_stmt_t stmt);
loxc_analysis_mutation_t
  loxc_analysis_stmt_mutation(loxc_stmt_t stmt, size_t i);
size_t loxc_analysis_expr_count_mutations(loxc_expr_t expr);
loxc_analysis_mutation_t
  loxc_analysis_expr_mutation(loxc_expr_t expr, size_t i);

loxc_typeset_t loxc_analysis_expr_type(loxc_expr_t expr);

#endif
