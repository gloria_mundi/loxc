
#ifndef LOXC_AST_H
#define LOXC_AST_H

#include <stdbool.h>

#include "lex.h"
#include "string.h"
#include "metadata.h"

typedef struct loxc_ast *loxc_ast_t;

typedef enum {
	LOXC_EXPR_NIL,
	LOXC_EXPR_BOOL,
	LOXC_EXPR_NUMBER,
	LOXC_EXPR_BINARY,
	LOXC_EXPR_UNARY,
	LOXC_EXPR_VARIABLE,
	LOXC_EXPR_ASSIGN,
} loxc_expr_type_t;

typedef struct loxc_expr {
	loxc_metadata_t metadata;
	loxc_span_t position;
	loxc_expr_type_t type;
} *loxc_expr_t;

typedef struct loxc_expr_bool {
	struct loxc_expr expr;
	bool value;
} *loxc_expr_bool_t;

typedef struct loxc_expr_number {
	struct loxc_expr expr;
	double value;
} *loxc_expr_number_t;

typedef enum {
	LOXC_EXPR_ADD,
	LOXC_EXPR_SUB,
	LOXC_EXPR_MUL,
	LOXC_EXPR_DIV,
	LOXC_EXPR_ARITH,

	LOXC_EXPR_LT = LOXC_EXPR_ARITH,
	LOXC_EXPR_LE,
	LOXC_EXPR_GT,
	LOXC_EXPR_GE,

	LOXC_EXPR_EQ,
	LOXC_EXPR_NE,

	LOXC_EXPR_AND,
	LOXC_EXPR_OR,
} loxc_expr_binary_op_t;

typedef struct loxc_expr_binary {
	struct loxc_expr expr;
	loxc_expr_binary_op_t op;
	loxc_expr_t left, right;
} *loxc_expr_binary_t;

typedef enum {
	LOXC_EXPR_NEG,
	LOXC_EXPR_NOT,
} loxc_expr_unary_op_t;

typedef struct loxc_expr_unary {
	struct loxc_expr expr;
	loxc_expr_unary_op_t op;
	loxc_expr_t arg;
} *loxc_expr_unary_t;

typedef struct loxc_expr_variable {
	struct loxc_expr expr;
	loxc_string_t name;
} *loxc_expr_variable_t;

typedef struct loxc_expr_assign {
	struct loxc_expr expr;
	loxc_string_t name;
	loxc_expr_t value;
} *loxc_expr_assign_t;

loxc_expr_t loxc_expr_new_nil(loxc_span_t position);
loxc_expr_t loxc_expr_new_bool(loxc_span_t position, bool value);
loxc_expr_t loxc_expr_new_number(loxc_span_t position, double value);
loxc_expr_t loxc_expr_new_binary(loxc_span_t position,
  loxc_expr_binary_op_t op, loxc_expr_t left, loxc_expr_t right);
loxc_expr_t loxc_expr_new_unary(loxc_span_t position,
  loxc_expr_unary_op_t op, loxc_expr_t arg);
loxc_expr_t loxc_expr_new_variable(loxc_span_t position, loxc_string_t name);
loxc_expr_t loxc_expr_new_assign(loxc_span_t position,
  loxc_string_t name, loxc_expr_t value);

void loxc_expr_free(loxc_expr_t);
loxc_string_t loxc_expr_to_string(loxc_expr_t);

typedef enum {
	LOXC_STMT_NULL,
	LOXC_STMT_EXPR,
	LOXC_STMT_PRINT,
	LOXC_STMT_DECLARE,
	LOXC_STMT_BLOCK,
	LOXC_STMT_IF,
} loxc_stmt_type_t;

typedef struct loxc_stmt {
	loxc_metadata_t metadata;
	loxc_span_t position;
	loxc_stmt_type_t type;
} *loxc_stmt_t;

typedef struct loxc_stmt_expr {
	struct loxc_stmt stmt;
	loxc_expr_t expr;
} *loxc_stmt_expr_t;

typedef struct loxc_stmt_print {
	struct loxc_stmt stmt;
	loxc_expr_t expr;
} *loxc_stmt_print_t;

typedef struct loxc_stmt_declare {
	struct loxc_stmt stmt;
	loxc_string_t name;
	loxc_expr_t value;
} *loxc_stmt_declare_t;

typedef struct loxc_stmt_block {
	struct loxc_stmt stmt;
	loxc_ast_t inner;
} *loxc_stmt_block_t;

typedef struct loxc_stmt_if {
	struct loxc_stmt stmt;
	loxc_expr_t condition;
	loxc_stmt_t if_, else_;
} *loxc_stmt_if_t;

loxc_stmt_t loxc_stmt_new_null(loxc_span_t position);
loxc_stmt_t loxc_stmt_new_expr(loxc_span_t position, loxc_expr_t expr);
loxc_stmt_t loxc_stmt_new_print(loxc_span_t position, loxc_expr_t expr);
loxc_stmt_t loxc_stmt_new_declare(loxc_span_t position,
  loxc_string_t name, loxc_expr_t expr);
loxc_stmt_t loxc_stmt_new_block(loxc_span_t position, loxc_ast_t ast);
loxc_stmt_t loxc_stmt_new_if(loxc_span_t position,
  loxc_expr_t condition, loxc_stmt_t if_, loxc_stmt_t else_);

void loxc_stmt_free(loxc_stmt_t);
loxc_string_t loxc_stmt_to_string(loxc_stmt_t);

loxc_ast_t loxc_ast_new(void);
void loxc_ast_append_stmt(loxc_ast_t, loxc_stmt_t);
unsigned int loxc_ast_count_stmts(loxc_ast_t);
loxc_stmt_t loxc_ast_get_stmt(loxc_ast_t, unsigned int index);
loxc_metadata_t loxc_ast_metadata(loxc_ast_t);

void loxc_ast_free(loxc_ast_t);
loxc_string_t loxc_ast_to_string(loxc_ast_t);

#endif
