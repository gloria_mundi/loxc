
#include <stdbool.h>

#include "parse.h"
#include "ast.h"
#include "lex.h"
#include "macros.h"

typedef struct parser {
	loxc_logger_t logger;
	loxc_lexer_t lexer;
	loxc_token_t token;
} *parser_t;

static void panic(parser_t parser);
static bool consume(parser_t, loxc_token_type_t expect, const char *desc);
static void expect(parser_t, const char *expected);
static bool match(parser_t, loxc_token_type_t expect);
static bool check(parser_t, loxc_token_type_t expect);
static loxc_token_type_t peek(parser_t);
static loxc_token_type_t advance(parser_t);

static loxc_stmt_t declaration(parser_t);

static loxc_expr_t parse_expr(parser_t);

loxc_ast_t loxc_parse(loxc_lexer_t lexer, loxc_logger_t logger) {
	struct parser parser = {
		.logger = logger,
		.lexer = lexer,
		.token = loxc_lex(lexer),
	};

	loxc_ast_t ast = loxc_ast_new();

	bool ok = true;

	while (!match(&parser, LOXC_TOKEN_EOF)) {
		loxc_stmt_t stmt = declaration(&parser);

		if (stmt != NULL) {
			loxc_ast_append_stmt(ast, stmt);
			continue;
		} else {
			ok = false;
		}
	}

	if (ok) return ast;

	loxc_ast_free(ast);
	return NULL;
}

/*** Statement parser ***/

static loxc_stmt_t statement(parser_t);
static loxc_stmt_t declare_var(parser_t);
static loxc_stmt_t block(parser_t);
static loxc_stmt_t if_(parser_t);

static loxc_stmt_t declaration(parser_t parser) {
	if (check(parser, LOXC_TOKEN_VAR)) return declare_var(parser);

	loxc_stmt_t stmt = statement(parser);
	if (stmt != NULL) return stmt;

	panic(parser);
	return NULL;
}

static loxc_stmt_t statement(parser_t parser) {
	if (check(parser, LOXC_TOKEN_LEFT_BRACE)) return block(parser);
	if (check(parser, LOXC_TOKEN_IF)) return if_(parser);
	if (check(parser, LOXC_TOKEN_ELSE)) {
		loxc_logger_error_position(parser->logger,
		  loxc_lexer_source(parser->lexer), parser->token.position,
		  "`else' without `if'");
		return NULL;
	}

	loxc_position_t start = parser->token.position.start;

	loxc_stmt_t (*makestmt)(loxc_span_t, loxc_expr_t);
	if (match(parser, LOXC_TOKEN_PRINT)) makestmt = &loxc_stmt_new_print;
	else makestmt = &loxc_stmt_new_expr;

	loxc_expr_t expr = parse_expr(parser);
	if (expr == NULL) return NULL;

	loxc_position_t end = parser->token.position.end;

	if (consume(parser, LOXC_TOKEN_SEMICOLON,
	  "`;' or an infix or postfix operator")) {
		return makestmt(loxc_span_new(start, end), expr);
	}

	loxc_expr_free(expr);
	return NULL;
}

static loxc_stmt_t declare_var(parser_t parser) {
	loxc_position_t start = parser->token.position.start;
	advance(parser);

	if (!check(parser, LOXC_TOKEN_IDENTIFIER)) {
		expect(parser, "identifier");
		return NULL;
	}

	loxc_string_t name = parser->token.value.string;
	advance(parser);

	loxc_expr_t value;
	loxc_position_t end;
	if (match(parser, LOXC_TOKEN_EQUAL)) {
		value = parse_expr(parser);
		if (value == NULL) {
			loxc_string_free(name);
			return NULL;
		}

		end = parser->token.position.end;
		if (!consume(parser, LOXC_TOKEN_SEMICOLON,
		  "`;' or an infix or postfix operator")) {
			loxc_string_free(name);
			loxc_expr_free(value);
			return NULL;
		}
	} else {
		end = parser->token.position.end;
		if (!consume(parser, LOXC_TOKEN_SEMICOLON, "`=' or `;'")) {
			loxc_string_free(name);
			return NULL;
		}

		value = loxc_expr_new_nil(loxc_span_none());
	}

	return loxc_stmt_new_declare(loxc_span_new(start, end), name, value);
}

static loxc_stmt_t block(parser_t parser) {
	loxc_span_t opening = parser->token.position;
	advance(parser);

	bool ok = true;
	loxc_ast_t ast = loxc_ast_new();

	while (!check(parser, LOXC_TOKEN_RIGHT_BRACE)) {
		if (check(parser, LOXC_TOKEN_EOF)) {
			loxc_logger_error_position(parser->logger,
			  loxc_lexer_source(parser->lexer),
			  opening, "Unclosed block");
			loxc_ast_free(ast);
			return NULL;
		}

		loxc_stmt_t stmt = declaration(parser);

		if (stmt != NULL) loxc_ast_append_stmt(ast, stmt);
		else ok = false;
	}

	loxc_position_t end = parser->token.position.end;
	advance(parser);

	if (ok) return loxc_stmt_new_block(loxc_span_new(opening.start, end),
	                                                                  ast);

	loxc_ast_free(ast);
	return NULL;
}

static loxc_stmt_t if_(parser_t parser) {
	loxc_position_t start = parser->token.position.start;
	advance(parser);

	if (!consume(parser, LOXC_TOKEN_LEFT_PAREN, "`(' after `if'"))
	  return NULL;

	loxc_expr_t condition = parse_expr(parser);
	if (condition == NULL) return NULL;

	if (!consume(parser, LOXC_TOKEN_RIGHT_PAREN,
	  "`)' or an infix or postfix operator")) {
		loxc_expr_free(condition);
		return NULL;
	}

	loxc_stmt_t if_ = statement(parser);
	if (if_ == NULL) {
		loxc_expr_free(condition);
		return NULL;
	}

	loxc_stmt_t else_;
	loxc_position_t end;
	if (match(parser, LOXC_TOKEN_ELSE)) {
		else_ = statement(parser);
		if (else_ == NULL) {
			loxc_expr_free(condition);
			loxc_stmt_free(if_);
			return NULL;
		}
		end = else_->position.end;
	} else {
		else_ = loxc_stmt_new_null(loxc_span_none());
		end = if_->position.end;
	}

	return
	  loxc_stmt_new_if(loxc_span_new(start, end), condition, if_, else_);
}

/*** Expression parser ***/
typedef enum {
	PREC_EXPR,
	PREC_ASSIGN,
	PREC_OR,
	PREC_AND,
	PREC_EQUAL,
	PREC_COMPARE,
	PREC_SUM,
	PREC_PRODUCT,
	PREC_PRIMARY,
} precedence_t;

static loxc_expr_t parse_prec(parser_t, precedence_t surrounding);
static loxc_expr_t nil(parser_t, precedence_t);
static loxc_expr_t boolean(parser_t, precedence_t);
static loxc_expr_t number(parser_t, precedence_t);
static loxc_expr_t variable(parser_t, precedence_t);
static loxc_expr_t group(parser_t, precedence_t);
static loxc_expr_t unary(parser_t, precedence_t);
static loxc_expr_t binop(parser_t, precedence_t, loxc_expr_t);
static loxc_expr_t noplus(parser_t, precedence_t);
static loxc_expr_t noassign(parser_t, precedence_t, loxc_expr_t);

static const struct {
	loxc_expr_t (*prefix)(parser_t, precedence_t surrounding);
	loxc_expr_t (*infix)(parser_t, precedence_t surrounding,
	  loxc_expr_t lhs);
	precedence_t prec;
} prec_table[LOXC_TOKEN_N_TYPES] = {
	[LOXC_TOKEN_LEFT_PAREN]        = { group,    NULL,     PREC_EXPR     },
	[LOXC_TOKEN_PLUS]              = { noplus,   binop,    PREC_SUM      },
	[LOXC_TOKEN_MINUS]             = { unary,    binop,    PREC_SUM      },
	[LOXC_TOKEN_ASTERISK]          = { NULL,     binop,    PREC_PRODUCT  },
	[LOXC_TOKEN_SOLIDUS]           = { NULL,     binop,    PREC_PRODUCT  },
	[LOXC_TOKEN_EXCLAMATION]       = { unary,    NULL,     PREC_EXPR     },
	[LOXC_TOKEN_EQUAL_EQUAL]       = { NULL,     binop,    PREC_EQUAL    },
	[LOXC_TOKEN_EXCLAMATION_EQUAL] = { NULL,     binop,    PREC_EQUAL    },
	[LOXC_TOKEN_LESS]              = { NULL,     binop,    PREC_COMPARE  },
	[LOXC_TOKEN_LESS_EQUAL]        = { NULL,     binop,    PREC_COMPARE  },
	[LOXC_TOKEN_GREATER]           = { NULL,     binop,    PREC_COMPARE  },
	[LOXC_TOKEN_GREATER_EQUAL]     = { NULL,     binop,    PREC_COMPARE  },
	[LOXC_TOKEN_EQUAL]             = { NULL,     noassign, PREC_ASSIGN   },
	[LOXC_TOKEN_AND]               = { NULL,     binop,    PREC_AND      },
	[LOXC_TOKEN_OR]                = { NULL,     binop,    PREC_OR       },
	[LOXC_TOKEN_NIL]               = { nil,      NULL,     PREC_EXPR     },
	[LOXC_TOKEN_TRUE]              = { boolean,  NULL,     PREC_EXPR     },
	[LOXC_TOKEN_FALSE]             = { boolean,  NULL,     PREC_EXPR     },
	[LOXC_TOKEN_NUMBER]            = { number,   NULL,     PREC_EXPR     },
	[LOXC_TOKEN_IDENTIFIER]        = { variable, NULL,     PREC_EXPR     },
};

static inline loxc_expr_t parse_expr(parser_t parser) {
	return parse_prec(parser, PREC_EXPR);
}

static loxc_expr_t parse_prec(parser_t parser, precedence_t surrounding) {
	loxc_token_type_t type = peek(parser);
	if (prec_table[type].prefix == NULL) {
		expect(parser, "a primary or a prefix operator");
		return NULL;
	}

	loxc_expr_t expr = (*prec_table[type].prefix)(parser, surrounding);
	if (expr == NULL) return NULL;

	type = peek(parser);
	while (prec_table[type].prec > surrounding) {
		loxc_expr_t new =
		  (*prec_table[type].infix)(parser, surrounding, expr);
		if (new == NULL) {
			/* error */
			return NULL;
		}

		expr = new;
		type = peek(parser);
	}

	return expr;
}

static loxc_expr_t nil(parser_t parser, precedence_t surrounding) {
	loxc_expr_t expr = loxc_expr_new_nil(parser->token.position);
	advance(parser);
	return expr;
}

static loxc_expr_t boolean(parser_t parser, precedence_t surrounding) {
	loxc_expr_t expr = loxc_expr_new_bool(parser->token.position,
	  parser->token.type == LOXC_TOKEN_TRUE);
	advance(parser);
	return expr;
}

static loxc_expr_t number(parser_t parser, precedence_t surrounding) {
	loxc_expr_t expr = loxc_expr_new_number(parser->token.position,
	  parser->token.value.number);
	advance(parser);
	return expr;
}

static loxc_expr_t variable(parser_t parser, precedence_t surrounding) {
	loxc_string_t name = parser->token.value.string;
	loxc_span_t position = parser->token.position;
	advance(parser);
	if (surrounding < PREC_ASSIGN && match(parser, LOXC_TOKEN_EQUAL)) {
		loxc_expr_t value = parse_prec(parser, PREC_ASSIGN - 1);
		if (value == NULL) {
			loxc_string_free(name);
			return NULL;
		}

		return loxc_expr_new_assign(
		  loxc_span_new(position.start, value->position.end),
		  name, value);
	}

	return loxc_expr_new_variable(position, name);
}

static loxc_expr_t group(parser_t parser, precedence_t surrounding) {
	loxc_position_t start = parser->token.position.start;
	advance(parser);
	loxc_expr_t expr = parse_prec(parser, PREC_EXPR);
	if (expr == NULL) return NULL;
	loxc_position_t end = parser->token.position.end;
	if (consume(parser, LOXC_TOKEN_RIGHT_PAREN,
	  "`)' or an infix or postfix operator")) {
		expr->position.start = start;
		expr->position.end = end;
		return expr;
	}

	loxc_expr_free(expr);
	return NULL;
}

static loxc_expr_t unary(parser_t parser, precedence_t surrounding) {
	loxc_position_t start = parser->token.position.start;
	loxc_token_type_t ttype = advance(parser);
	loxc_expr_t arg = parse_prec(parser, PREC_PRIMARY);
	if (arg == NULL) return NULL;

	loxc_expr_unary_op_t op;
	switch (ttype) {
		case LOXC_TOKEN_MINUS: op = LOXC_EXPR_NEG; break;
		case LOXC_TOKEN_EXCLAMATION: op = LOXC_EXPR_NOT; break;
		default: unreachable();
	}
	return loxc_expr_new_unary(loxc_span_new(start, arg->position.end),
	  op, arg);
}

static loxc_expr_t noplus(parser_t parser, precedence_t surrounding) {
	loxc_logger_error_position(parser->logger,
	  loxc_lexer_source(parser->lexer), parser->token.position,
	  "Lox doesn't have an unary plus operator");
	return NULL;
}

static loxc_expr_t noassign(parser_t parser, precedence_t surrounding,
  loxc_expr_t lhs) {
	loxc_logger_error_position(parser->logger,
	  loxc_lexer_source(parser->lexer),
	  loxc_span_new(lhs->position.start, parser->token.position.end),
	  "Illegal assignment target");
	loxc_expr_free(lhs);
	return NULL;
}

static loxc_expr_t binop(parser_t parser, precedence_t surrounding,
  loxc_expr_t lhs) {
	loxc_token_type_t ttype = advance(parser);
	loxc_expr_t rhs = parse_prec(parser, prec_table[ttype].prec);
	if (rhs == NULL) {
		loxc_expr_free(lhs);
		return NULL;
	}

	loxc_expr_binary_op_t op;
	switch (ttype) {
		case LOXC_TOKEN_PLUS: op = LOXC_EXPR_ADD; break;
		case LOXC_TOKEN_MINUS: op = LOXC_EXPR_SUB; break;
		case LOXC_TOKEN_ASTERISK: op = LOXC_EXPR_MUL; break;
		case LOXC_TOKEN_SOLIDUS: op = LOXC_EXPR_DIV; break;

		case LOXC_TOKEN_EQUAL_EQUAL: op = LOXC_EXPR_EQ; break;
		case LOXC_TOKEN_EXCLAMATION_EQUAL: op = LOXC_EXPR_NE; break;

		case LOXC_TOKEN_LESS: op = LOXC_EXPR_LT; break;
		case LOXC_TOKEN_LESS_EQUAL: op = LOXC_EXPR_LE; break;
		case LOXC_TOKEN_GREATER: op = LOXC_EXPR_GT; break;
		case LOXC_TOKEN_GREATER_EQUAL: op = LOXC_EXPR_GE; break;

		case LOXC_TOKEN_AND: op = LOXC_EXPR_AND; break;
		case LOXC_TOKEN_OR: op = LOXC_EXPR_OR; break;

		default: unreachable();
	}
	return loxc_expr_new_binary(
	  loxc_span_new(lhs->position.start, rhs->position.end),
	  op, lhs, rhs);
}

/*** Parser primitives ***/
static void panic(parser_t parser) {
	loxc_token_type_t type;
	do {
		type = peek(parser);
		if (type == LOXC_TOKEN_EOF || type == LOXC_TOKEN_RIGHT_BRACE)
		  break;
		loxc_token_free(parser->token);
		advance(parser);
	} while (type != LOXC_TOKEN_SEMICOLON);
}

static bool consume(parser_t parser, loxc_token_type_t expected,
  const char *desc) {
	if (match(parser, expected)) return true;
	else {
		expect(parser, desc);
		return false;
	}
}

static void expect(parser_t parser, const char *desc) {
	loxc_logger_error_position(parser->logger,
	  loxc_lexer_source(parser->lexer), parser->token.position,
	  "Expected %s, found %s",
	    desc, loxc_token_type_name(peek(parser)));
}

static bool match(parser_t parser, loxc_token_type_t expect) {
	if (check(parser, expect)) {
		advance(parser);
		return true;
	} else return false;
}

static bool check(parser_t parser, loxc_token_type_t expect) {
	return peek(parser) == expect;
}

static inline loxc_token_type_t peek(parser_t parser) {
	return parser->token.type;
}

static loxc_token_type_t advance(parser_t parser) {
	loxc_token_type_t type = peek(parser);
	if (type != LOXC_TOKEN_EOF) parser->token = loxc_lex(parser->lexer);
	return type;
}
