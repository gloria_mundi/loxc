
#ifndef LOXC_MEM_H
#define LOXC_MEM_H

#include <stdlib.h>

/* These macros are not prefixed with loxc_ because they're purely internal */

#define mem_alloc malloc
#define mem_realloc realloc
#define mem_free free

// Memory management of single values
#define mem_new(T) ((T *) mem_alloc(sizeof(T)))

// Memory management of arrays
#define mem_new_array(T, N) ((T *) mem_alloc((N) * sizeof(T)))
#define mem_resize_array(V, N) mem_realloc(V, (N) * sizeof *(V))

// Memory management of structs with flexible array members
#define mem_new_with_array(T, ARR, N) \
	((T *) mem_alloc(sizeof(T) + (N) * sizeof(ARR)))
#define mem_resize_with_array(V, ARR, N) \
	mem_realloc(V, sizeof *(V) + (N) * sizeof(ARR))

#endif
