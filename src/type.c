
#include <stdint.h>
#include <stdbool.h>

#include "type.h"
#include "mem.h"

struct loxc_typeset {
	unsigned short ref;
	loxc_primitive_set_t primitives;
};

static loxc_typeset_t create(void) {
	loxc_typeset_t typeset = mem_new(struct loxc_typeset);
	typeset->ref = 1;
	return typeset;
}

loxc_typeset_t loxc_typeset_empty(void) {
	loxc_typeset_t typeset = create();
	typeset->primitives = LOXC_PRIMITIVE_SET_EMPTY;
	return typeset;
}

loxc_typeset_t loxc_typeset_primitive(loxc_primitive_t primitive) {
	loxc_typeset_t typeset = create();
	typeset->primitives = LOXC_PRIMITIVE_SET(primitive);
	return typeset;
}

loxc_typeset_t loxc_typeset_union(loxc_typeset_t old1, loxc_typeset_t old2) {
	loxc_typeset_t new = create();
	new->primitives = old1->primitives | old2->primitives;
	return new;
}

loxc_typeset_t loxc_typeset_limit_falsy(loxc_typeset_t old) {
	loxc_typeset_t new = create();
	new->primitives =
	  old->primitives & (LOXC_PRIMITIVE_SET_NIL | LOXC_PRIMITIVE_SET_BOOL);
	return new;
}

loxc_typeset_t loxc_typeset_limit_truthy(loxc_typeset_t old) {
	loxc_typeset_t new = create();
	new->primitives = old->primitives & ~LOXC_PRIMITIVE_SET_NIL;
	return new;
}

void loxc_typeset_ref(loxc_typeset_t typeset) { typeset->ref++; }
void loxc_typeset_unref(loxc_typeset_t typeset) {
	if (--typeset->ref > 0) return;

	mem_free(typeset);
}

bool loxc_typeset_is_primitive(loxc_typeset_t typeset) {
	return true;
}

loxc_primitive_set_t loxc_typeset_primitive_set(loxc_typeset_t typeset) {
	return typeset->primitives;
}
