
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <llvm-c/Core.h>
#include <llvm-c/Analysis.h>
#include <llvm-c/TargetMachine.h>

#include "logger.h"
#include "version.h"
#include "slurp.h"
#include "string.h"
#include "mem.h"
#include "ast.h"
#include "lex.h"
#include "parse.h"
#include "analysis.h"
#include "codegen.h"

static bool compile_program(loxc_logger_t logger, const char *filename);
static void show_info(const char *program_name);
static LLVMTargetMachineRef get_host_target_machine(loxc_logger_t);

int main(int argc, char **argv) {
	int status;
	switch (argc) {
		case 0:
			show_info("loxc");
			status = 0;
			break;
		case 1:
			show_info(argv[0]);
			status = 0;
			break;
		case 2: {
			loxc_logger_t logger = loxc_logger_new("loxc");
			status = !compile_program(logger, argv[1]);
			loxc_logger_free(logger);
			break;
		}
		default:
			show_info(argv[0]);
			status = 1;
			break;
	}

	/* This is kinda useless, but I like my Valgrind reports clean. */
	LLVMShutdown();
	return status;
}

static void show_info(const char *progname) {
	printf("\
loxc version %s, compiled at %s %s\n\
License: GNU GPLv3+  (GNU General Public License, version 3 or later)\n\
Usage: %s <filename>\n\
",
	  loxc_version_string(), loxc_compilation_date(),
	  loxc_compilation_time(), progname);
}

static bool compile_program(loxc_logger_t logger, const char *filename) {
	loxc_string_t code;
	const char *logname;

	size_t basenamelen;
	{
		const char *dot = strrchr(filename, '.');
		if (dot == NULL) basenamelen = strlen(filename);
		else basenamelen = dot - filename;
	}

	if (strcmp(filename, "-") == 0) {
		logname = "<stdin>";
		code = loxc_slurp(logger, stdin, logname);
	} else {
		logname = filename;

		FILE *f = fopen(filename, "r");
		if (f == NULL) {
			loxc_logger_error(logger, "Could not open `%s'",
			  logname);
			return false;
		}
		code = loxc_slurp(logger, f, logname);
		fclose(f);
	}

	if (loxc_string_error(code)) return false;

	loxc_source_t source = loxc_source_new(logname, code);
	if (source == NULL) {
		loxc_logger_out_of_memory(logger);
		loxc_string_free(code);
		return false;
	}

	loxc_lexer_t lexer = loxc_lexer_new(source, logger);
	if (lexer == NULL) {
		loxc_logger_out_of_memory(logger);
		loxc_source_free(source);
		loxc_string_free(code);
		return false;
	}

	loxc_ast_t ast = loxc_parse(lexer, logger);
	bool ok = loxc_lexer_free(lexer);

	if (ast == NULL) {
		loxc_source_free(source);
		loxc_string_free(code);
		return false;
	}

	loxc_analysis_t analysis = loxc_analyse(ast, source, logger);
	if (analysis == NULL) {
		loxc_ast_free(ast);
		loxc_source_free(source);
		loxc_string_free(code);
		return false;
	}

	LLVMTargetMachineRef machine = get_host_target_machine(logger);
	if (machine == NULL) {
		loxc_analysis_free(analysis);
		loxc_ast_free(ast);
		loxc_source_free(source);
		loxc_string_free(code);
		return false;
	}

	LLVMModuleRef module = LLVMModuleCreateWithName(logname);

	LLVMTargetDataRef data_layout = LLVMCreateTargetDataLayout(machine);
	LLVMSetModuleDataLayout(module, data_layout);
	LLVMDisposeTargetData(data_layout);

	char *triple = LLVMGetTargetMachineTriple(machine);
	LLVMSetTarget(module, triple);
	LLVMDisposeMessage(triple);

	loxc_codegen_to_module(module, analysis, ast, source);

	loxc_analysis_free(analysis);
	loxc_ast_free(ast);
	loxc_source_free(source);
	loxc_string_free(code);

	char *llvmfilename = mem_new_array(char, basenamelen+4);
	memcpy(llvmfilename, filename, basenamelen);
	memcpy(llvmfilename + basenamelen, ".ll", 4);
	LLVMPrintModuleToFile(module, llvmfilename, NULL);
	mem_free(llvmfilename);

	char *error = NULL;
	if (LLVMVerifyModule(module, LLVMReturnStatusAction, &error)) {
		loxc_logger_error(logger,
		  "LLVM verification failed: %s", error);
		LLVMDisposeMessage(error);
		LLVMDisposeTargetMachine(machine);
		LLVMDisposeModule(module);
		return false;
	}

	LLVMDisposeMessage(error);

	char *objfilename = mem_new_array(char, basenamelen + 3);
	memcpy(objfilename, filename, basenamelen);
	memcpy(objfilename + basenamelen, ".o", 3);

	error = NULL;
	if (LLVMTargetMachineEmitToFile(machine, module,
	  objfilename, LLVMObjectFile, &error)) {
		loxc_logger_error(logger, "Object file emission failed: %s",
		  error);
		LLVMDisposeMessage(error);
		LLVMDisposeTargetMachine(machine);
		LLVMDisposeModule(module);
		mem_free(objfilename);
		return false;
	}

	LLVMDisposeMessage(error);

	LLVMDisposeTargetMachine(machine);
	LLVMDisposeModule(module);
	mem_free(objfilename);

	return ok;
}

static LLVMTargetMachineRef get_host_target_machine(loxc_logger_t logger) {
	char *triple = LLVMGetDefaultTargetTriple();

	LLVMInitializeNativeTarget();
	LLVMInitializeNativeAsmPrinter();
	LLVMTargetRef target;
	char *error = NULL;
	if (LLVMGetTargetFromTriple(triple, &target, &error)) {
		loxc_logger_error(logger,
		  "Could not get native LLVM target: %s", error);
		LLVMDisposeMessage(triple);
		return NULL;
	}
	LLVMDisposeMessage(error);

	if (!LLVMTargetHasTargetMachine(target)) {
		loxc_logger_error(logger,
		  "No LLVM target machine found for target %s", triple);
		LLVMDisposeMessage(triple);
		return NULL;
	}

	LLVMTargetMachineRef machine = LLVMCreateTargetMachine(target,
	  triple, "", "", /* No specific CPU or features */
	  LLVMCodeGenLevelDefault, LLVMRelocDynamicNoPic, LLVMCodeModelDefault
	);

	LLVMDisposeMessage(triple);

	return machine;
}
