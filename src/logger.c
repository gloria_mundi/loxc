
#include <stdio.h>
#include <stdarg.h>

#include "logger.h"
#include "mem.h"

struct loxc_logger {
	loxc_logger_t parent;
	const char *name;
};

loxc_logger_t loxc_logger_new(const char *name) {
	loxc_logger_t logger = mem_new(struct loxc_logger);
	if (logger != NULL) {
		logger->parent = NULL;
		logger->name = name;
	}
	return logger;
}

loxc_logger_t loxc_logger_sub(loxc_logger_t parent, const char *name) {
	loxc_logger_t logger = mem_new(struct loxc_logger);
	if (logger != NULL) {
		logger->parent = parent;
		logger->name = name;
	}
	return logger;
}

void loxc_logger_free(loxc_logger_t logger) {
	mem_free(logger);
}

static void write_names(loxc_logger_t logger) {
	if (logger->parent != NULL) write_names(logger->parent);
	fprintf(stderr, "%s: ", logger->name);
}

void loxc_logger_error(loxc_logger_t logger, const char *format, ...) {
	write_names(logger);

	va_list ap;
	va_start(ap, format);
	vfprintf(stderr, format, ap);
	va_end(ap);

	fputc('\n', stderr);
}

void loxc_logger_error_position(loxc_logger_t logger,
  loxc_source_t source, loxc_span_t position, const char *format, ...) {
	write_names(logger);

	fprintf(stderr, "%s:%u: ", loxc_source_filename(source),
	  position.start.line);

	va_list ap;
	va_start(ap, format);
	vfprintf(stderr, format, ap);
	va_end(ap);
	putc('\n', stderr);

	for (unsigned int lineno = position.start.line;
	  lineno <= position.end.line; lineno++) {
		loxc_string_t line = loxc_source_line(source, lineno);
		fwrite(line.chars, 1, line.length, stderr);

		unsigned int i = 0;
		if (lineno == position.start.line)
		  for (; i < position.start.column; i++) {
			if (line.chars[i] == '\t') putc('\t', stderr);
			else putc(' ', stderr);
		}

		unsigned int end_column;
		if (lineno == position.end.line)
		  end_column = position.end.column;
		else end_column = line.length - 1;

		for (; i < end_column; i++) {
			if (line.chars[i] == '\t') putc('\t', stderr);
			else putc('^', stderr);
		}

		putc('\n', stderr);
	}
}

void loxc_logger_out_of_memory(loxc_logger_t logger) {
	write_names(logger);

	fputs("Out of memory!\n", stderr);
}
