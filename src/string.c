
#include <stdlib.h>
#include <stdbool.h>
#include <stdarg.h>
#include <string.h>
#include <stdio.h>

#include "string.h"
#include "mem.h"

loxc_string_t loxc_string_from_cstring(char *str) {
	return (loxc_string_t) { strlen(str), str };
}

#undef loxc_string_new
loxc_string_t loxc_string_new(char *chars, size_t len) {
	return (loxc_string_t) { len, chars };
}

#undef loxc_string_new_error
loxc_string_t loxc_string_new_error(void) {
	return (loxc_string_t) { 0, NULL };
}

void loxc_string_free(loxc_string_t string) {
	mem_free(string.chars);
}

char *loxc_string_to_cstring(loxc_string_t string) {
	char *cstring = mem_new_array(char, string.length + 1);
	memcpy(cstring, string.chars, string.length);
	cstring[string.length] = '\0';
	return cstring;
}

#undef loxc_string_error
bool loxc_string_error(loxc_string_t string) {
	return string.chars == NULL;
}

#undef loxc_string_length
size_t loxc_string_length(loxc_string_t string) {
	return string.length;
}

#undef loxc_string_chars
char *loxc_string_chars(loxc_string_t string) {
	return string.chars;
}

bool loxc_string_eq(loxc_string_t lhs, loxc_string_t rhs) {
	return lhs.length == rhs.length &&
	  memcmp(lhs.chars, rhs.chars, lhs.length) == 0;
}

loxc_string_t loxc_string_copy(loxc_string_t string) {
	if (string.chars != NULL) {
		char *orig = string.chars;
		string.chars = mem_new_array(char, string.length);
		if (string.chars != NULL)
		  memcpy(string.chars, orig, string.length);
	}
	return string;
}

loxc_string_t loxc_string_concat(loxc_string_t lhs, loxc_string_t rhs) {
	loxc_string_t result;
	result.length = lhs.length + rhs.length;
	result.chars = mem_new_array(char, result.length);
	if (result.chars != NULL) {
		memcpy(result.chars, lhs.chars, lhs.length);
		memcpy(result.chars + lhs.length, rhs.chars, rhs.length);
	}
	return result;
}

loxc_string_t loxc_string_append(loxc_string_t lhs, loxc_string_t rhs) {
	size_t lhs_len = lhs.length;
	lhs.length += rhs.length;
	if (lhs.chars != NULL) {
		lhs.chars = mem_resize_array(lhs.chars, lhs.length);
		if (lhs.chars != NULL)
		  memcpy(lhs.chars + lhs_len, rhs.chars, rhs.length);
	}
	return lhs;
}

loxc_string_t loxc_string_range(loxc_string_t string,
  size_t start, size_t end) {
	if (end > string.length) end = string.length;
	if (string.chars != NULL) string.chars += start;
	string.length = end - start;
	return string;
}

void loxc_string_builder_init(loxc_string_builder_t *builder) {
	builder->out_of_memory = false;
	builder->cap = 0;
	builder->string = loxc_string_new(NULL, 0);
}

void loxc_string_builder_cancel(loxc_string_builder_t *builder) {
	loxc_string_free(builder->string);
}

loxc_string_t loxc_string_builder_complete(loxc_string_builder_t *builder) {
	if (builder->string.chars != NULL || builder->out_of_memory)
	  return builder->string;
	return loxc_string_new(mem_new_array(char, 1), 0);
}

static char *make_space(loxc_string_builder_t *builder, size_t space) {
	if (builder->out_of_memory) return NULL;

	size_t old_len = builder->string.length;
	size_t new_len = old_len + space;
	if (new_len > builder->cap) {
		size_t cap = builder->cap;
		if (cap == 0) cap = 0x20;
		while (new_len > cap) cap *= 2;
		char *new = mem_resize_array(builder->string.chars, cap);
		if (new == NULL) {
			builder->out_of_memory = true;
			mem_free(builder->string.chars);
			builder->string.chars = NULL;
			return NULL;
		}

		builder->cap = cap;
		builder->string.chars = new;
	}

	builder->string.length = new_len;
	return builder->string.chars + old_len;
}

void loxc_string_builder_append(loxc_string_builder_t *builder,
  loxc_string_t string) {
	char *space = make_space(builder, string.length);
	if (space != NULL) memcpy(space, string.chars, string.length);
}

void loxc_string_builder_appendf(loxc_string_builder_t *builder,
  const char *format, ...) {
	va_list ap;
	va_start(ap, format);
	loxc_string_builder_vappendf(builder, format, ap);
	va_end(ap);
}

void loxc_string_builder_vappendf(loxc_string_builder_t *builder,
  const char *format, va_list ap) {
	va_list aq;
	va_copy(aq, ap);
	size_t len = vsnprintf(NULL, 0, format, aq) + 1;
	va_end(aq);

	char *space = make_space(builder, len);
	if (space != NULL) {
		builder->string.length--;
		vsnprintf(space, len, format, ap);
	}
}
