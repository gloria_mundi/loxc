
#include <stdint.h>
#include <assert.h>

#include "hashmap.h"
#include "string.h"
#include "mem.h"

typedef uint_least32_t hash_t;

typedef struct entry {
	hash_t hash;
	loxc_string_t key;
	void *value;
} *entry_t;

struct loxc_hashmap {
	size_t len, cap;
	entry_t entries;
};

struct loxc_hashmap_iter {
	loxc_hashmap_t map;
	size_t pos;
	bool initial;
	bool destructive;
};

static entry_t find(loxc_hashmap_t map, loxc_string_t key, hash_t hash);
static hash_t hash_str(loxc_string_t);

loxc_hashmap_t loxc_hashmap_new(void) {
	loxc_hashmap_t map = mem_new(struct loxc_hashmap);
	map->len = map->cap = 0;
	map->entries = NULL;
	return map;
}

void loxc_hashmap_free(loxc_hashmap_t map) {
	mem_free(map->entries);
	mem_free(map);
}

static void set_cap(loxc_hashmap_t map, size_t new_cap) {
	map->cap = new_cap - 1;
	entry_t entries = map->entries = mem_new_array(struct entry, new_cap);
	for (size_t i = 0; i < new_cap; i++) entries[i].value = NULL;
}

void *loxc_hashmap_set(loxc_hashmap_t map, loxc_string_t key, void *value) {
	if (map->len * 3 >= map->cap * 2) {
		if (map->cap == 0) {
			set_cap(map, 0x10);
		} else {
			size_t old_cap = map->cap + 1;
			entry_t old = map->entries;
			set_cap(map, 2 * old_cap);
			for (size_t i = 0; i < old_cap; i++)
			  loxc_hashmap_set(map, old[i].key, old[i].value);
			mem_free(old);
		}
	}

	hash_t hash = hash_str(key);
	entry_t entry = find(map, key, hash);

	void *old_value = entry->value;
	if (old_value == NULL) {
		entry->hash = hash;
		entry->key = key;
		map->len++;
	}

	entry->value = value;
	return old_value;
}

void *loxc_hashmap_get(loxc_hashmap_t map, loxc_string_t key) {
	if (map->entries == NULL) return NULL;

	hash_t hash = hash_str(key);
	entry_t entry = find(map, key, hash);
	return entry->value;
}

static entry_t find(loxc_hashmap_t map, loxc_string_t key, hash_t hash) {
	for (size_t pos = hash & map->cap;; pos = (pos + 1) & map->cap) {
		entry_t entry = &map->entries[pos];

		if (entry->value == NULL ||
		  (entry->hash == hash && loxc_string_eq(entry->key, key))) {
			return entry;
		}
	}
}

static loxc_hashmap_iter_t create_iter(loxc_hashmap_t map, bool destructive) {
	loxc_hashmap_iter_t iter = mem_new(struct loxc_hashmap_iter);
	iter->map = map;
	iter->pos = 0;
	iter->initial = map->entries != NULL;
	iter->destructive = destructive;
	return iter;
}

loxc_hashmap_iter_t loxc_hashmap_iter(loxc_hashmap_t map) {
	return create_iter(map, false);
}

loxc_hashmap_iter_t loxc_hashmap_free_iter(loxc_hashmap_t map) {
	return create_iter(map, true);
}

void loxc_hashmap_iter_free(loxc_hashmap_iter_t iter) {
	if (iter->destructive) loxc_hashmap_free(iter->map);
	mem_free(iter);
}

loxc_string_t loxc_hashmap_iter_next(loxc_hashmap_iter_t iter) {
	loxc_hashmap_t map = iter->map;
	size_t pos = iter->pos;
	if (iter->initial) iter->initial = false;
	else pos++;
	for (; pos <= map->cap; pos++) {
		if (map->entries[pos].value != NULL) {
			iter->pos = pos;
			return map->entries[pos].key;
		}
	}
	iter->pos = map->cap;
	return loxc_string_new_error();
}

void *loxc_hashmap_iter_set(loxc_hashmap_iter_t iter, void *new) {
	entry_t entry = &iter->map->entries[iter->pos];
	void *old = entry->value;
	assert(old != NULL);
	entry->value = new;
	return old;
}

void *loxc_hashmap_iter_get(loxc_hashmap_iter_t iter) {
	void *value = iter->map->entries[iter->pos].value;
	assert(value != NULL);
	return value;
}

/* Fowler-Noll-Vo 1a */
static hash_t hash_str(loxc_string_t string) {
	const unsigned char *chars = (const unsigned char *) string.chars;
	hash_t hash = 2166136261;
	for (size_t i = 0; i < string.length; i++)
	  hash = (hash ^ chars[i]) * 16777619;
	return hash;
}
