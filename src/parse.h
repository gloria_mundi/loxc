
#ifndef LOXC_PARSE_H
#define LOXC_PARSE_H

#include "ast.h"

loxc_ast_t loxc_parse(loxc_lexer_t lexer, loxc_logger_t logger);

#endif
