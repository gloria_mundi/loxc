
#ifndef LOXC_LOGGER_H
#define LOXC_LOGGER_H

typedef struct loxc_logger *loxc_logger_t;

#include "lex.h"

loxc_logger_t loxc_logger_new(const char *name);
loxc_logger_t loxc_logger_sub(loxc_logger_t, const char *name);
void loxc_logger_free(loxc_logger_t);

void loxc_logger_error(loxc_logger_t, const char *format, ...);
void loxc_logger_error_position(loxc_logger_t,
  loxc_source_t source, loxc_span_t position, const char *format, ...);
void loxc_logger_out_of_memory(loxc_logger_t);

#endif
