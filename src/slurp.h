
#ifndef LOXC_SLURP_H
#define LOXC_SLURP_H

#include <stdio.h>

#include "string.h"
#include "logger.h"

loxc_string_t loxc_slurp(loxc_logger_t logger, FILE *f, const char *filename);

#endif
