
#ifndef LOXC_LEX_H
#define LOXC_LEX_H

#include <stdbool.h>

#include "string.h"

typedef struct loxc_source *loxc_source_t;
typedef struct loxc_lexer *loxc_lexer_t;

typedef struct loxc_position {
	unsigned int line, column;
} loxc_position_t;

typedef struct loxc_span {
	loxc_position_t start, end;
} loxc_span_t;

typedef enum {
	/* Single character */
	LOXC_TOKEN_LEFT_PAREN, LOXC_TOKEN_RIGHT_PAREN,
	LOXC_TOKEN_LEFT_BRACE, LOXC_TOKEN_RIGHT_BRACE,
	LOXC_TOKEN_PLUS, LOXC_TOKEN_MINUS,
	LOXC_TOKEN_ASTERISK, LOXC_TOKEN_SOLIDUS,
	LOXC_TOKEN_EXCLAMATION,
	LOXC_TOKEN_EQUAL_EQUAL, LOXC_TOKEN_EXCLAMATION_EQUAL,
	LOXC_TOKEN_LESS, LOXC_TOKEN_LESS_EQUAL,
	LOXC_TOKEN_GREATER, LOXC_TOKEN_GREATER_EQUAL,
	LOXC_TOKEN_SEMICOLON, LOXC_TOKEN_EQUAL,

	/* Keywords */
	LOXC_TOKEN_PRINT, LOXC_TOKEN_VAR,
	LOXC_TOKEN_IF, LOXC_TOKEN_ELSE,
	LOXC_TOKEN_AND, LOXC_TOKEN_OR,
	LOXC_TOKEN_NIL, LOXC_TOKEN_TRUE, LOXC_TOKEN_FALSE,

	/* Tokens with a value */
	LOXC_TOKEN_NUMBER, LOXC_TOKEN_IDENTIFIER,

	/* Special */
	LOXC_TOKEN_EOF,

	/* Sentinel */
	LOXC_TOKEN_N_TYPES
} loxc_token_type_t;

typedef struct loxc_token {
	loxc_span_t position;
	loxc_token_type_t type;
	union {
		double number;
		loxc_string_t string;
	} value;
} loxc_token_t;

#include "logger.h"

loxc_source_t loxc_source_new(const char *filename, loxc_string_t code);
void loxc_source_free(loxc_source_t);
const char *loxc_source_filename(loxc_source_t);
loxc_string_t loxc_source_code(loxc_source_t);
loxc_string_t loxc_source_line(loxc_source_t, unsigned int line);

const char *loxc_token_type_name(loxc_token_type_t);
loxc_string_t loxc_token_data(loxc_token_t token);
void loxc_token_free(loxc_token_t token);

loxc_lexer_t loxc_lexer_new(loxc_source_t source, loxc_logger_t logger);
bool loxc_lexer_free(loxc_lexer_t);
loxc_source_t loxc_lexer_source(loxc_lexer_t);
loxc_token_t loxc_lex(loxc_lexer_t);

loxc_span_t loxc_span_new(loxc_position_t start, loxc_position_t end);
loxc_span_t loxc_span_none(void);
#define loxc_span_new(S, E) ((loxc_span_t) { S, E })
#define loxc_span_none() ((loxc_span_t) { {0, 0}, {0, 0} })

#endif
