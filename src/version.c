
#include "version.h"

uint_least32_t loxc_version(void) { return LOXC_VERSION; }
const char *loxc_version_string(void) { return LOXC_VERSION_STRING; }
const char *loxc_compilation_date(void) { return __DATE__; }
const char *loxc_compilation_time(void) { return __TIME__; }
