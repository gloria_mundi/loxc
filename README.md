# LLVM compiler for the Lox programming language

![Pipeline Status](https://gitlab.com/gloria_mundi/loxc/badges/master/pipeline.svg)

`loxc` is an LLVM-based compiler for the Lox programming language from Bob
Nystrom's [*Crafting Interpreters*](https://craftinginterpreters.com/).

It is a work-in-progress, and far from complete:
- Statements:
  - [X] Expression
  - [ ] Control flow
    - [X] `if`/`else`
    - [ ] `for`
    - [ ] `while`
  - [X] `print`
  - [X] Blocks `{...}`
- Expressions:
  - [ ] Assignment (`=`)
    - [X] Simple (`variable = value`)
    - [ ] Member (`object.member = value`)
  - [X] Control flow (`and`, `or`)
  - [X] Equality (`==`, `!=`)
  - [X] Comparison (`>`, `>=`, `<`, `<=`)
  - [X] Arithmetic (`+`, `-`, `*`, `/`)
  - [X] Logic (`!`)
  - [ ] Function call (`function(...)`)
  - [ ] Member access (`object.member`)
    - [ ] Super access (`super.member`)
  - [X] Constants
  - [X] Variable access
- [X] Variables
  - [X] Global
  - [X] Local
- Data types:
  - [X] Numbers
  - [X] Booleans
  - [ ] Strings
  - [ ] First-class classes
  - [ ] First-class functions
  - [X] Nil
- [ ] Functions
- [ ] Classes

## Building

Dependencies:
- `make`
- a C compiler
- LLVM development files (libraries, headers, `llc`)

Building

	make

This will produce:
- The `loxc` binary
- `libloxc.a`, the compiler library
- `liblox.a`, the runtime library

## Running

	./loxc <filename.lox>

This will produce `filename.ll` (for debugging purposes, might disable this
later) and `filename.o`.

You will then need to link the object file with the runtime library,
`liblox.a`, which in turn requires the C runtime library. The easiest way
to do that is using a C compiler:

	cc -o <executable> <filename.o> liblox.a
