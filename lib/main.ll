
declare ccc void @lox_c_printf(i8 *, ...)
declare ccc void @lox_c_write(i8 *, i32)
declare ccc void @lox_c_panic(i8 *, i32) noreturn
declare fastcc void @"lox::main"()

define ccc void @lox_c_main() {
	call fastcc void @"lox::main"()
	ret void
}

@num_format = internal constant [4 x i8] c"%g\0A\00"
define fastcc void @"lox::print(number)"(double %value) {
	%format = getelementptr [4 x i8], [4 x i8]* @num_format, i32 0, i32 0
	call ccc void(i8 *, ...) @lox_c_printf(i8* %format, double %value)
	ret void
}

@true_text = internal constant [5 x i8] c"true\0A"
@false_text = internal constant [6 x i8] c"false\0A"
define fastcc void @"lox::print(bool)"(i1 %value) {
	br i1 %value, label %true, label %false

true:
	%true_text = getelementptr [5 x i8], [5 x i8]* @true_text, i32 0, i32 0
	call ccc void @lox_c_write(i8 *%true_text, i32 5)
	ret void

false:
	%false_text =
	  getelementptr [6 x i8], [6 x i8]* @false_text, i32 0, i32 0
	call ccc void @lox_c_write(i8 *%false_text, i32 6)
	ret void
}

@nil_text = internal constant [4 x i8] c"nil\0A"
define fastcc void @"lox::print(nil)"() {
	%text = getelementptr [4 x i8], [4 x i8]* @nil_text, i32 0, i32 0
	call ccc void @lox_c_write(i8 *%text, i32 4)
	ret void
}

define fastcc void @"lox::panic"(i8 *%msg, i32 %len) noreturn {
	call ccc void @lox_c_panic(i8 *%msg, i32 %len) noreturn
	ret void
}
