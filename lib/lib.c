
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdarg.h>

extern void lox_c_main(void);

int main(void) {
	lox_c_main();
}

void lox_c_printf(const char *format, ...) {
	va_list ap;
	va_start(ap, format);
	vprintf(format, ap);
	va_end(ap);
}

void lox_c_write(const char *msg, uint32_t length) {
	fwrite(msg, 1, length, stdout);
}

void lox_c_panic(const char *msg, uint32_t length) {
	fwrite(msg, 1, length, stderr);
	putc('\n', stderr);
	exit(127);
}
