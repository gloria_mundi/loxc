
.POSIX :

include config.mk

build: compiler library

compiler:
	+cd src/ ; $(MAKE)
	cp src/$(PROGNAME) src/lib$(PROGNAME).a .

library:
	+cd lib/ ; $(MAKE)
	cp lib/lib$(LANGNAME).a .

clean:
	+cd src/ ; $(MAKE) clean
	+cd lib/ ; $(MAKE) clean
